#!/usr/bin/env bash
rm -rf /var/www/eibo/dist
tar -xf /var/www/eibo/build.tar -C /var/www/eibo/
rm /var/www/eibo/build.tar
