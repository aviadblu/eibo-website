#!/usr/bin/env bash
# ssh aviad@52.174.53.118 /home/aviad/eibo-website/devOps/deploy.sh
# ssh aviad@52.174.53.118 'bash -s' < /Users/aviad/code/eibo-website/devOps/deploy.sh

tar -cvf build.tar ./dist
rm -rf ./dist
tar -xvf build.tar
scp -r /Users/aviad/code/eibo-website/build.tar aviad@52.174.53.118:/home/aviad/eibo-website-build/build.tar
ssh aviad@52.174.53.118 /home/aviad/eibo-website-build/deploy.sh



cd /home/aviad/eibo-website
git checkout master
git pull origin master
npm install
npm rebuild node-sass --force
ng build --prod
rm -rf /home/aviad/eibo-website/build && mkdir /home/aviad/eibo-website/build && cp -r /home/aviad/eibo-website/dist/* /home/aviad/eibo-website/build
sudo cp -R /home/aviad/eibo-website/build /opt/eibo-website
