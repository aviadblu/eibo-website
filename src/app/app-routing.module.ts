import { MyLibraryComponent } from './client/client-u-profile/my-library/my-library.component';
import { UserProfileComponent } from './client/client-u-profile/profile/user-profile.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientAboutComponent } from './client/client-about/client-about.component';
import { ClientAdminMainComponent } from './client/client-admin-main/client-admin-main.component';
import { ClientAdminComponent } from './client/client-admin/client-admin.component';
import { ClientContactComponent } from './client/client-contact/client-contact.component';
import { ClientHomepageComponent } from './client/client-homepage/client-homepage.component';
import { ClientMainComponent } from './client/client-main/client-main.component';
import { ClientProfileComponent } from './client/client-profile/client-profile.component';
import { ClientUProfileComponent } from './client/client-u-profile/client-u-profile.component';
import { ClientUComponent } from './client/client-u/client-u.component';
import {ClientChatsListComponent} from "./client/client-chats-list/client-chats-list.component";
import {ClientChatComponent} from "./client/client-chat/client-chat.component";
import { CommunitiesComponent } from 'app/client/client-u-profile/communities/communities.component';
import { ClientBookComponent } from 'app/client/client-book/client-book.component';
import { ClientReportComponent } from 'app/client/client-report/client-report.component';
import {ClientCommunityComponent} from "./client/client-u-profile/communities/client-community/client-community.component";
import {CreateCommunityComponent} from "./client/create-community/create-community.component";
import {GroupSearchComponent} from "./client/group-search/group-search.component";
import {UserLeavesGuard} from "./guards/user-leaves-guard.guard";
import {LoginGuard} from "./guards/login-guard.guard";

const routes: Routes = [
  {
    path: 'app',
    component: ClientMainComponent,
    children: [
      {
        path: 'home',
        component: ClientHomepageComponent
      },
      {
        path: 'about',
        component: ClientAboutComponent
      },
      {
        path: 'contact',
        component: ClientContactComponent
      },
      {
        path: 'report',
        component: ClientReportComponent
      },
      {
        path: 'profile/:uid',
        component: ClientProfileComponent
      },
      {
        path: 'book/:bookId',
        component: ClientBookComponent
      },
      {
        path: 'find-groups',
        component: GroupSearchComponent
      },
      {
        path: 'u',
        component: ClientUComponent,
        children: [
          {
            path: 'profile',
            component: ClientUProfileComponent,
            children: [
              {
                path:'profile',
                component: UserProfileComponent
              },
              {
                path:'library',
                component: MyLibraryComponent,
                canDeactivate: [UserLeavesGuard]
              },
              {
                path:'communities',
                component: CommunitiesComponent
              },
              {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
              }
            ]
          },
          {
            path: 'community/:communityId',
            component: ClientCommunityComponent
          },
          {
            path: 'create/community',
            component: CreateCommunityComponent
          },
          {
            path: 'update/community/:communityId',
            component: CreateCommunityComponent
          },
          {
            path: 'chats',
            component: ClientChatsListComponent
          },
          {
            path: 'chat/:chatId',
            component: ClientChatComponent
          },
          {
            path: 'chat',
            component: ClientChatComponent
          },
          {
            path: '',
            redirectTo: 'profile',
            pathMatch: 'full'
          }
        ],
        canActivate: [LoginGuard]
      },
      {
        path: 'admin',
        component: ClientAdminComponent,
        children: [
          {
            path: 'main',
            component: ClientAdminMainComponent
          },
          {
            path: '',
            redirectTo: 'main',
            pathMatch: 'full'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ],
    canDeactivate: [UserLeavesGuard]
  },
  {
    path: '',
    redirectTo: 'app/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
