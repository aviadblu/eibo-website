import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatSnackBarModule,
  MatTabsModule,
  MatGridListModule,
  MatAutocompleteModule,
  MatTableModule, MatSlideToggleModule, MatChipsModule, MatSelectModule, MatStepperModule, MatProgressBarModule,
  MatButtonToggleModule, MatTooltip, MatBadgeModule
} from "@angular/material";
import {LayoutModule} from '@angular/cdk/layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  imports: [
    MatSidenavModule,
    MatMenuModule,
    MatGridListModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatInputModule,
    MatSnackBarModule,
    MatTabsModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTableModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatSelectModule,
    MatStepperModule,
    MatTooltipModule,
    LayoutModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatButtonToggleModule
  ],
  exports: [
    MatSidenavModule,
    MatMenuModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatInputModule,
    MatSnackBarModule,
    MatGridListModule,
    MatTabsModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTableModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatSelectModule,
    MatStepperModule,
    MatTooltipModule,
    LayoutModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatButtonToggleModule
  ]
})
export class MaterialModule {
}
