import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {DialogAlertComponent} from "../shared/components/dialog-alert/dialog-alert.component";
import {MatDialog} from "@angular/material";

export interface ComponentCanDeactivate {
  canDeactivate: ($event?) => boolean | Observable<boolean>;
}

@Injectable()
export class UserLeavesGuard implements CanDeactivate<any> {
  constructor(private dialog: MatDialog) {

  }

  canDeactivate(component: ComponentCanDeactivate): Observable<boolean> | Promise<boolean> | boolean {
    return this.dialog.openDialogs.length == 0;
  }
}
