import { TestBed, async, inject } from '@angular/core/testing';

import { UserLeavesGuard } from './user-leaves-guard.guard';

describe('UserLeavesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserLeavesGuard]
    });
  });

  it('should ...', inject([UserLeavesGuard], (guard: UserLeavesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
