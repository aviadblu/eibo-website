import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {FacebookService} from "../shared/services/facebook.service";
import {map, skip} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private fbService :FacebookService,
              private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.fbService.isLoggedIn$.getValue() === true ||
      this.fbService.isLoggedIn$.pipe(skip(1)).pipe(map((val) => {
      if(val !== true) {
        this.router.navigate(['/'], {queryParams: {redirect: state.url}});
      }
      return val === true;
    }));
  }
}
