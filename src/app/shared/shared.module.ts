import {NgModule} from '@angular/core';
import {ComponentsModule} from "./components/components.module";
import {LangService} from "./services/lang.service";
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { ReadMoreDirective } from './read-more.directive';
import {CommonModule} from "@angular/common";
import { SwiperModule } from 'angular2-useful-swiper';

@NgModule({
  imports: [
    ComponentsModule,
    CommonModule,
    SwiperModule
  ],
  providers: [LangService],
  declarations: [
    TimeAgoPipe,
    ReadMoreDirective
  ],
  exports: [ComponentsModule,
    TimeAgoPipe,
    ReadMoreDirective,
    SwiperModule
  ]
})
export class SharedModule {
}
