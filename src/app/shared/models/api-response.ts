export interface ApiResponse {
  count: number,
  data: Array<any>
}
