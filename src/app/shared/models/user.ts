export interface IUser {
  uid: string;
  auth_uid: string;
  address?: IAddress;
  name: string;
  email: string;
  photo: string;
  last_update: Date;
}

export interface IAddress {
  formatted_address: string;
  lat: Number;
  lng: Number;
}

export interface IUserInfo {
  uid: string;
  displayName: string;
  email: string;
  photoURL: string;
}
