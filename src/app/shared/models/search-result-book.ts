export class SearchResultBook {
  id: number;
  name: string;
  author: string;
  picture: string;
}
