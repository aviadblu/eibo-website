import {User} from "../../client/types/user.module";

export class Book {
  id?: number;
  name: string;
  author: string;
  cover_url?: string;
  picture?: string;
  category :string;
  bookuserid?: number;
  rating: number;
  review: string;
  holders: User[];

  constructor(id: number, name: string, author: string, cover_url: string, picture: string, rating: number) {
    this.id = id;
    this.name = name;
    this.author = author;
    this.cover_url = cover_url;
    this.picture = picture;
    this.rating = rating;
  }
}
