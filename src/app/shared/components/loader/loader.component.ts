import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'eibo-loader',
  template: `
    <div class="client-site-content loader-wrapper">
      <div class="mdl-grid client-max-width" loader>
        <mat-spinner color="warm"></mat-spinner>
      </div>
    </div>`,
  styleUrls: ['./loader.component.less']
})
export class LoaderComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
