import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'eibo-star-display',
  templateUrl: './star-display.component.html',
  styleUrls: ['./star-display.component.less']
})
export class StarDisplayComponent{
  stars:string[] = [];

  @Input() numberOfStars:number;
  _rating:number;

  @Input() set rating(value) {
    this._rating = value;
    this.stars = [];
    this.initStars(this._rating, this.numberOfStars)
  }
  get rating() {
    return this._rating;
  }

  constructor() {}

  private initStars(rating:number,numberOfStars:number):void {
    rating = Math.round(rating*2)/2;
    for(let i=0;i<numberOfStars;i++) {
      if(rating-i==0.5)
        this.stars.push("star_half");
      else if(rating-i>0)
        this.stars.push("star_rate");
      else
        this.stars.push("star_border");
    }
  }
}
