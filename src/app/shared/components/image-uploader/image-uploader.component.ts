import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { CloudinaryOptions, CloudinaryUploader } from "ng2-cloudinary";
import { CloudinaryHelperService } from "../../services/cloudinary-helper.service";
import { GeneralService } from "../../services/general.service";

@Component({
  selector: "eibo-image-uploader",
  templateUrl: "./image-uploader.component.html",
  styleUrls: ["./image-uploader.component.less"]
})
export class ImageUploaderComponent implements OnInit {
  @Input() image: string;
  @Output() onImageChange = new EventEmitter<boolean>();
  uploadInProcess: boolean = false;
  @ViewChild("imageUrl") imageUrl;
  @ViewChild("fileInput") fileInput;
  @Input() styles;

  constructor(private gSvc: GeneralService) {
    this.uploader.onSuccessItem = (
      item: any,
      response: string,
      status: number,
      headers: any
    ): any => {
      let res: any = JSON.parse(response);
      this.image = CloudinaryHelperService.getUrlWithImageResize(res.secure_url, 92, 140);
      this.updateImage(res.secure_url);
      this.uploadInProcess = false;
      return { item, response, status, headers };
    };
  }

  ngOnInit() {}

  public uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(CloudinaryHelperService.options)
  );

  openFilesBrowser() {
    if (this.uploadInProcess) {
      return;
    }
    this.fileInput.nativeElement.click();
  }

  public upload(): void {
    if (this.uploadInProcess) {
      return;
    }
    this.uploadInProcess = true;
    this.uploader.uploadAll();
  }

  private updateImage(url) {
    this.image = url;
    this.onImageChange.emit(url);
  }

  uploadFromUrl(url) {
    if (this.uploadInProcess) {
      return;
    }

    if (GeneralService.isUrlValid(url)) {
      this.uploadInProcess = true;
      this.gSvc.uploadImageFromUrl(url).then(res => {
        this.image = CloudinaryHelperService.getUrlWithImageResize(res['url'], 92, 140);
        this.updateImage(res["url"]);
        this.uploadInProcess = false;
        this.imageUrl.nativeElement.value = "";
      });
    }
  }
}
