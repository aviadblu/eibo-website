import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BarecodeScannerLivestreamComponent} from "ngx-barcode-scanner";
import {Observable, Subject} from "rxjs";
import {distinctUntilChanged} from "rxjs/internal/operators";
import {MatSnackBar, MatSnackBarRef, SimpleSnackBar} from "@angular/material";

@Component({
  selector: 'eibo-barcode-scanner',
  templateUrl: './barcode-scanner.component.html',
  styleUrls: ['./barcode-scanner.component.less']
})
export class BarcodeScannerComponent implements OnInit {

  @ViewChild(BarecodeScannerLivestreamComponent)
  BarcodeScanner: BarecodeScannerLivestreamComponent;
  public barcodeDisplay: string;
  @Output() barcodeScanned : EventEmitter<string> = new EventEmitter<string>();
  barcodeEmit$;
  private snakbarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(private snakbar: MatSnackBar) {
    this.barcodeEmit$ = new Subject()
      .pipe(distinctUntilChanged());

    this.barcodeEmit$.subscribe((val) => {
        this.barcodeScanned.emit(val);
        this.hideBarcode();
      });
  }

  ngOnInit() {
  }

  scanBarcode() {
    this.BarcodeScanner.start();
    this.barcodeDisplay = 'block';
    this.snakbarRef = this.snakbar.open('Scan your book\'s barcode', 'OK')
  }

  hideBarcode() {
    this.BarcodeScanner.stop();
    this.barcodeDisplay = 'none';
    this.snakbarRef.dismiss();
  }

  onValueChanges(value){
    this.barcodeEmit$.next(value.code);
  }

}
