import { Component, Input, EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'eibo-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.less']
})
export class StarRatingComponent {

  @Input() rating:number = 0;
  @Input() numberOfStars:number = 5;
  @Output() onRated = new EventEmitter<number>();
  stars:number[] = [];

  constructor() {
    this.initStars(this.numberOfStars);
  }

  private initStars(numberOfStars:number):void {
    for(let i=0;i<numberOfStars;i++) {
      this.stars.push(i+1);
    }
  }

  onClickStar(rating):void {
    this.rating = Math.round(rating*2)/2;
    this.onRated.emit(rating);
  }

}
