import {StarDisplayComponent} from './star-display/star-display.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MaterialModule} from '../../material/material.module';
import {LoaderComponent} from './loader/loader.component';
import {StarRatingComponent} from './star-rating/star-rating.component';
import {DialogDeleteComponent} from './dialog-delete/dialog-delete.component'
import {CheckSignComponent} from './check-sign/check-sign.component';
import {ImageUploaderComponent} from './image-uploader/image-uploader.component';
import {FileUploadModule} from "ng2-file-upload";
import {DialogAlertComponent} from './dialog-alert/dialog-alert.component'
import {BarecodeScannerLivestreamModule} from "ngx-barcode-scanner";
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { InfoDialogComponent } from './info-dialog/info-dialog.component';
import { BannerDisplayComponent } from './banner-display/banner-display.component';
import {BookReviewDialogComponent} from "../../client/book-review-dialog/book-review-dialog.component";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BarecodeScannerLivestreamModule,
    FileUploadModule
  ],
  entryComponents: [
    DialogDeleteComponent,
    DialogAlertComponent,
    ErrorDialogComponent,
    InfoDialogComponent,
    BookReviewDialogComponent
  ],
  declarations: [LoaderComponent,
    StarDisplayComponent,
    StarRatingComponent,
    CheckSignComponent,
    DialogDeleteComponent,
    ImageUploaderComponent,
    DialogAlertComponent,
    ErrorDialogComponent,
    InfoDialogComponent,
    BannerDisplayComponent
  ],
  exports: [LoaderComponent,
    StarDisplayComponent,
    StarRatingComponent,
    CheckSignComponent,
    DialogDeleteComponent,
    ImageUploaderComponent,
    BarecodeScannerLivestreamModule,
    DialogAlertComponent,
    BannerDisplayComponent]
})
export class ComponentsModule {
}
