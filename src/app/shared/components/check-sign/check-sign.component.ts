import {Component, Input} from '@angular/core';

@Component({
  selector: 'eibo-check-sign',
  template: `
    <mat-icon doneIcon *ngIf="checked">done</mat-icon>
  `,
  styles: [`
    [doneIcon] {
      color: green;
    }
  `]
})
export class CheckSignComponent {
  @Input() checked: boolean;

  constructor() {
  }
}
