import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {Subject} from "rxjs/index";

@Component({
  selector: 'eibo-dialog-alert',
  templateUrl: './dialog-alert.component.html',
  styleUrls: ['./dialog-alert.component.less']
})
export class DialogAlertComponent implements OnInit {
  caption: string;
  private confirmed$: Subject<void> = new Subject<void>();
  constructor(public dialogRef: MatDialogRef<DialogAlertComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.caption = data.caption;
    this.confirmed$.subscribe(data.onConfirmed);
  }

  confirm(route) {
    this.dialogRef.close(route);
    if(route != false){
      this.confirmed$.next();
    }
  }
  close() {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
