import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Observable, Subject} from "rxjs/index";

@Component({
  selector: 'eibo-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.less']
})
export class InfoDialogComponent implements OnInit {
  caption: string;
  InfoDescription: string;
  confirm$: Subject<void> = new Subject<void>();

  constructor(public dialogRef: MatDialogRef<InfoDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.caption = data.caption;
    this.InfoDescription = data.description;
    this.confirm$.subscribe(data.onConfirm);
  }

  ngOnInit() {
  }

  confirm() {
    this.confirm$.next();
  }
}
