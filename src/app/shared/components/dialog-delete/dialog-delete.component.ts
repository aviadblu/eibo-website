import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'eibo-dialog-delete',
  templateUrl: './dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.less']
})
export class DialogDeleteComponent implements OnInit {
  caption: string;
  constructor(public dialogRef: MatDialogRef<DialogDeleteComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    this.caption = data.caption;
  }

  confirm(confirmed: boolean) {
    this.dialogRef.close(confirmed);
  }

  ngOnInit() {
  }

}
