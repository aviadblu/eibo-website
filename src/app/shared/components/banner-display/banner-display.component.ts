import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'eibo-banner-display',
  templateUrl: './banner-display.component.html',
  styleUrls: ['./banner-display.component.less']
})
export class BannerDisplayComponent implements OnInit {
  @Input() bannerImage :string;

  constructor() { }

  ngOnInit() {
  }

}
