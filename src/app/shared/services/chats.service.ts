import {Injectable} from '@angular/core';
import {FirebaseService} from "./firebase.service";
import {BehaviorSubject} from "rxjs";
import {CloudinaryHelperService} from "./cloudinary-helper.service";
import {UserService} from "./user.service";
import { HttpService } from './http.service';

@Injectable()
export class ChatsService {
  public chats$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public unread$: BehaviorSubject<number> = new BehaviorSubject(0);
  public chat$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  public initialized$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private user = null;
  private chatsData = {};

  constructor(
    private http: HttpService,
    private firebaseSvc: FirebaseService,
    private userSvc: UserService
  ) {
    this.init();
  }

  private init() {
    this.userSvc.user$.subscribe(userInfo => {
      if (userInfo && userInfo.uid) {
        this.user = userInfo;
        this.subscribeForUserChats(this.user.uid);
        this.subscribeUnreadIncomings(this.user.uid);
      }
    });
  }
  private subscribeForUserChats(uid) {
    this.firebaseSvc.fb.ref(`/users/${uid}/chats`).on("value", snapshot => {
      let chats = snapshot.val();
      if (chats) {
        this.chatsData = chats;

        let chatsArray = [];
        Object.keys(chats).forEach(chatId => {
          chatsArray.push(chats[chatId]);
        });
        chatsArray.sort((a, b) => {
          return b.lastUpdate - a.lastUpdate;
        });
        this.chats$.next(
          chatsArray.map(chat => {
            chat.thumbnail = CloudinaryHelperService.getUrlWithImageResize(
              chat.targetPhoto,
              40,
              40
            );
            return chat;
          })
        );
        this.initialized$.next(true);
      } else {
        this.initialized$.next(true);
      }
    });
  }

  private hash(s: string) {
    let hash = 0,
      i,
      chr;
    if (s.length === 0) return hash;
    for (i = 0; i < s.length; i++) {
      chr = s.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  private formatDate(timeStamp) {
    const date = new Date(timeStamp);
    return (
      date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear()
    );
  }

  // order of id's dose not matter here
  private createChatId(userId1, userId2) {
    let arr = [userId1, userId2];
    arr.sort();
    return FirebaseService.hashString(arr.join("-"));
  }

  private subscribeUnreadIncomings(userUid: string) {
    this.firebaseSvc.fb.ref(`/users/${userUid}/chats`).on("value", snapshot => {
      const chats = snapshot.val();
      if (chats) {
        this.chatsData = chats;
        let unreadSum = 0;
        Object.keys(chats).forEach(chatId => {
          unreadSum = unreadSum + chats[chatId].messages_counter;
        });
        this.unread$.next(unreadSum);
      }
    });
  }

  public checkCreateNewChat(targetUser) {
    const now = new Date().getTime();
    return new Promise(resolve => {
      let chatId = this.createChatId(this.user.uid, targetUser);
      let newChat = {
        id: chatId,
        creation: now,
        last_update: now,
        created_by: this.user.uid,
        user1: this.user.uid,
        user2: targetUser,
        messages_counter: 0,
        messages: []
      };

      let chatRef = this.firebaseSvc.fb.ref(`/chats`).child(chatId);
      chatRef.once("value", snapshot => {
        let chatData = snapshot.val();
        if (!chatData) {
          this.userSvc.loadUserDataPromise(targetUser).then(targetUserData => {
            let p1 = new Promise(resolve_p1 => {
              chatRef.set(newChat, resolve_p1);
            });
            let p2 = new Promise(resolve_p2 => {
              this.firebaseSvc.fb
                .ref(`/users/${this.user.uid}/chats`)
                .child(chatId)
                .set(
                  {
                    id: chatId,
                    targetId: targetUser,
                    targetPg: targetUserData["id"],
                    targetName: targetUserData["name"],
                    targetPhoto: targetUserData["photo"],
                    lastUpdate: now,
                    messages_counter: 0
                  },
                  resolve_p2
                );
            });
            let p3 = new Promise(resolve_p3 => {
              this.firebaseSvc.fb
                .ref(`/users/${targetUser}/chats`)
                .child(chatId)
                .set(
                  {
                    id: chatId,
                    targetId: this.user["uid"],
                    targetPg: this.user["id"],
                    targetName: this.user["name"],
                    targetPhoto: this.user["photo"],
                    lastUpdate: now,
                    messages_counter: 0
                  },
                  resolve_p3
                );
            });
            Promise.all([p1, p2, p3]).then(() => {
              resolve(chatId);
            });
          });
        } else {
          resolve(chatId);
        }
      });
    });
  }

  bookSwap(chatId: string, targetUser, book, senderName?) {
    return new Promise(resolve => {
      const newMessage = {
        type: "book",
        book: {
          id: book.bookId,
          name: book.bookName,
          author: book.bookAuthor,
          image: book.bookCover,
          rating: book.reviewScore
        },
        owner: {
          id: targetUser.id,
          pg: targetUser.pg,
          name: targetUser.name,
          photo: targetUser.photo
          //distance: targetUser.distance,
        },
        content: {
          message: "Hi, i want to swap with u"
        },
        sender: this.user.uid,
        seen: false
      };

      this.send(chatId, targetUser.id, newMessage).then(chatId => {
        console.info("send bookSwap completed===============");
        const newEmail = {
          book:{
            id:book.bookId,
            name:book.bookName,
            author:book.bookAuthor,
            cover_url:book.bookCover,
            review:book.bookReview
          },
          chatId:chatId,
          name:senderName,
          targetId:targetUser.pg
        }
        this.http.post(`api/contact/swap`,newEmail).subscribe(()=>{
          return resolve(chatId);
        });
      });
    });
  }

  message(chatId: string, targetUserUid: string, content) {
    return new Promise(resolve => {
      const newMessage = {
        type: "message",
        content: content,
        sender: this.user.uid,
        seen: false
      };
      this.send(chatId, targetUserUid, newMessage).then(chatId => {
        console.info("send message completed===============");
        return resolve(chatId);
      });
    });
  }

  send(chatId: string, targetUserUid: string, body) {
    return new Promise(resolve => {
      const now = new Date().getTime();
      body.time = now;

      let p1, p2, p3: Promise<any>;

      this.firebaseSvc.fb
        .ref(`/chats`)
        .child(chatId)
        .once("value", snapshot => {
          const chat = snapshot.val();
          let messages = chat.messages;
          if (!messages) {
            messages = [];
          }
          messages.push(body);
          p1 = new Promise(resolve => {
            this.firebaseSvc.fb
              .ref(`/chats`)
              .child(chatId)
              .update(
                {
                  last_update: now,
                  messages_counter: messages.length,
                  messages: messages
                },
                resolve
              );
          });
        });

      p2 = new Promise(resolve => {
        this.firebaseSvc.fb
          .ref(`/users/${this.user.uid}/chats`)
          .child(chatId)
          .update(
            {
              lastUpdate: now,
              messages_counter: 0,
              last_message: body.content.message
            },
            resolve
          );
      });

      this.firebaseSvc.fb
        .ref(`/users/${targetUserUid}/chats`)
        .child(chatId)
        .once("value", snapshot => {
          const userChat = snapshot.val();
          p3 = new Promise(resolve => {
            this.firebaseSvc.fb
              .ref(`/users/${targetUserUid}/chats`)
              .child(chatId)
              .update(
                {
                  lastUpdate: now,
                  messages_counter: userChat ? userChat['messages_counter'] + 1 : 1,
                  last_message: body.content.message
                },
                resolve
              );
          });
        });

      Promise.all([p1, p2, p3]).then(() => resolve(chatId));
    });
  }

  zeroCounter(chatId): Promise<any> {
    let chatRef = this.firebaseSvc.fb
      .ref(`/users/${this.user.uid}/chats`)
      .child(chatId);
    return new Promise(resolve => {
      chatRef.update(
        {
          messages_counter: 0
        },
        resolve
      );
    });
  }

  getChatData(chatId) {
    return this.chatsData[chatId] || null;
  }

  openChatConnection(chatId) {
    this.firebaseSvc.fb
      .ref(`/chats/${chatId}/messages`)
      .on("value", snapshot => {
        const messages = snapshot.val() || [];

        let dateMap = {};

        messages.forEach(message => {
          const dateHash = this.formatDate(message.time);
          if (!dateMap[dateHash]) {
            dateMap[dateHash] = {
              date: message.time,
              messages: []
            };
          }
          message.isMe = message.sender == this.user.uid;
          dateMap[dateHash].messages.push(message);
        });

        let resArr = [];
        Object.keys(dateMap).forEach(hash => {
          resArr.push(dateMap[hash]);
        });
        this.chat$.next(resArr);
      });
  }
}
