import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {Md5} from 'ts-md5/dist/md5';

@Injectable()
export class FirebaseService {
  private _firebaseInstance;
  private _fb;
  private FacebookProvider;

  constructor() {
    var config = {
      apiKey: "AIzaSyB_otodsd6twDZdUg6id8zb5C1LLPFwujc",
      authDomain: "eibo-13cf7.firebaseapp.com",
      databaseURL: "https://eibo-13cf7.firebaseio.com",
      projectId: "eibo-13cf7",
      storageBucket: "eibo-13cf7.appspot.com",
      messagingSenderId: "982303954013"
    };
    this._firebaseInstance = firebase;
    firebase.initializeApp(config);
    this._fb = firebase.database();
    this.prepareFacebookProvider();
  }

  private prepareFacebookProvider() {
    this.FacebookProvider = new this._firebaseInstance.auth.FacebookAuthProvider();
    this.FacebookProvider.addScope('public_profile');
    this.FacebookProvider.addScope('email');
    this.FacebookProvider.addScope('user_friends');
    //this.FacebookProvider.addScope('user_actions.books');
    this.FacebookProvider.setCustomParameters({
      'display': 'popup'
    });
  }

  get fb() {
    return this._fb;
  }

  get firebaseInstance() {
    return this._firebaseInstance;
  }

  public static hashString(string) {
    return Md5.hashStr(string);
  }

  public doFacebookLogin() {
    this._firebaseInstance.auth().signInWithRedirect(this.FacebookProvider);
  }

  public getRedirectResults() {
    return this._firebaseInstance.auth().getRedirectResult();
  }

  public logout() {
    return this._firebaseInstance.auth().signOut();
  }
}
