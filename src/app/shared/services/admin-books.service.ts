import { Injectable } from "@angular/core";
import {BehaviorSubject, merge, Observable} from "rxjs";
import { CloudinaryHelperService } from "./cloudinary-helper.service";
import { DataSource } from "@angular/cdk/collections";
import { HttpService } from "./http.service";
import {map, take} from "rxjs/internal/operators";

@Injectable()
export class AdminBooksService {
  private adminBooksDataSource;
  private adminPendingBooksDataSource;

  books$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  pendingBooks$: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(private http: HttpService) {
    this.adminBooksDataSource = new AdminBooksDataSource(this.books$);
    this.adminPendingBooksDataSource = new AdminBooksDataSource(
      this.pendingBooks$
    );
  }

  init() {
    return Promise.all([this.loadPendingBooksList(), this.loadBooksList()]);
  }

  private loadBooksList() {
    return new Promise(resolve => {
      this.http
        .get("/api/books")
        .pipe(take(1))
        .pipe(map(books => {
          books.map(book => {

            book.thumbnail = book.cover_url
               ? book.cover_url.indexOf("res.cloudinary.com") > 0
                ? CloudinaryHelperService.getUrlWithImageResize(
                    book.cover_url,
                    33,
                    50
                  )
                : book.cover_url
              : "";
          });
          return books;
        }))
        .subscribe(books => {
          this.books$.next(books);
          resolve();
        });
    });
  }

  private loadPendingBooksList() {
    return new Promise(resolve => {
      this.http
        .get("/api/books?pending=1")
        .pipe(take(1))
        .pipe(map(books => {
          books.map(book => {
            book.thumbnail = book.cover_url
               ? book.cover_url.indexOf("res.cloudinary.com") > 0
                ? CloudinaryHelperService.getUrlWithImageResize(
                    book.cover_url,
                    33,
                    50
                  )
                : book.cover_url
              : "";
          });
          return books;
        }))
        .subscribe(books => {
          this.pendingBooks$.next(books);
          resolve();
        });
    });
  }

  loadBookData(bookId) {
    return new Promise(resolve => {
      this.http
        .get(`/api/books/${bookId}`)
        .pipe(take(1))
        .pipe(map(book => {
          book.thumbnail = book.cover_url
             ? book.cover_url.indexOf("res.cloudinary.com") > 0
              ? CloudinaryHelperService.getUrlWithImageResize(
                  book.cover_url,
                  92,
                  140
                )
              : book.cover_url
            : "";
          return book;
        }))
        .subscribe(book => {
          resolve(book);
        });
    });
  }

  addBook(payload) {
    return new Promise(resolve => {
      this.http
        .post(`/api/books`, payload)
        .pipe(take(1))
        .subscribe(book => {
          this.loadPendingBooksList();
          this.loadBooksList().then(() => {
            resolve(book);
          });
        });
    });
  }

  updateBook(bookId, payload) {
    return new Promise(resolve => {
      this.http
        .put(`/api/books/${bookId}`, payload)
        .pipe(take(1))
        .subscribe(book => {
          this.loadPendingBooksList();
          this.loadBooksList().then(() => {
            resolve(book);
          });
        });
    });
  }

  deleteBook(bookId) {
    return new Promise(resolve => {
      this.http
        .delete(`/api/books/${bookId}`)
        .pipe(take(1))
        .subscribe(() => {
          this.loadPendingBooksList();
          this.loadBooksList().then(resolve);
        });
    });
  }

  getAdminBooksDataSource(pending = false) {
    return pending
      ? this.adminPendingBooksDataSource
      : this.adminBooksDataSource;
  }

  mergeBooks(bookIdToDelete, BookIdToReplace) {
    return new Promise(resolve => {
      this.http
        .put(`/api/books/merge/${bookIdToDelete}`, {
          replaceId: BookIdToReplace
        })
        .pipe(take(1))
        .subscribe(book => {
          this.loadPendingBooksList();
          this.loadBooksList().then(() => {
            resolve(book);
          });
        });
    });
  }
}

class AdminBooksDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject("");

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  constructor(private books$) {
    super();
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [this.books$, this._filterChange];

    return merge(...displayDataChanges).pipe(map(() => {
      let books = [];
      this.books$.pipe(take(1)).subscribe(booksRes => {
        books = booksRes;
      });
      return books.filter(book => {
        let searchStr = (book.name + book.author).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    }));
  }

  disconnect() {}
}
