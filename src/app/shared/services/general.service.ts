import { Injectable } from "@angular/core";
import { CloudinaryHelperService } from "./cloudinary-helper.service";
import { UserService } from "./user.service";
import { BehaviorSubject } from "rxjs";
import { HttpService } from "./http.service";
import { Md5 } from "ts-md5/dist/md5";
import {take} from "rxjs/internal/operators";
import {Suggestion} from "../../client/types/suggestion.module";

@Injectable()
export class GeneralService {
  userSuggestions$: BehaviorSubject<Suggestion[]> = new BehaviorSubject(null);
  searchByBook = false;

  constructor(private http: HttpService, private userSvc: UserService) {
  }

  loadUserSuggestions(uid, bookIdsNotToFetch? :number[]) {
    const id = uid ? uid : 0;
    let dontFetchTheseBooksParam = bookIdsNotToFetch? `?bookIdsNotToFetch=${bookIdsNotToFetch}` : '';
    this.http
      .get(`api/suggestions/${id}` + dontFetchTheseBooksParam)
      .pipe(take(1))
      .subscribe(booksRes => {
        let books = JSON.parse(JSON.stringify(booksRes));
        books.map(book => {
          book["thumbnail"] =
            book["bookCover"] &&
            book["bookCover"].indexOf("res.cloudinary.com") > 0
              ? CloudinaryHelperService.getUrlWithImageResize(
                  book["bookCover"],
                  170,
                  260
                )
              : book["bookCover"];
          book["users"].forEach(user => {
            user["thumbnail"] = CloudinaryHelperService.getUrlWithImageResize(
              user["photo"],
              40,
              40
            );
            return user;
          });
          return book;
        });
        this.userSuggestions$.next(books);
      });
  }

  public static isUrlValid(url) {
    const regex = new RegExp(
      /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
    );
    return !!url.match(regex);
  }

  public uploadImageFromUrl(url) {
    return new Promise((resolve, reject) => {
      this.http
        .post("/api/g/uploadToCloudinary", { url: url })
        .pipe(take(1))
        .subscribe(resolve, reject);
    });
  }

  public static hashString(string) {
    return Md5.hashStr(string);
  }
}
