import { Injectable } from "@angular/core";
import {BehaviorSubject, Subject} from "rxjs/index";
import {Community} from "./community.service";
import {HttpService} from "./http.service";
import {take} from "rxjs/internal/operators";

@Injectable()
export class AdminGroupsService {
  adminGroups$: BehaviorSubject<Community[]> = new BehaviorSubject<Community[]>(null);

  constructor(private http :HttpService) {
    http.get('/api/communities/pending/get').pipe(take(1))
      .subscribe((res) => this.adminGroups$.next(res.data));
  }

  approveCommunity(community: Community) {
    return this.http
      .get(`/api/communities/approve/${community.id}`)
      .pipe(take(1));
  }
}
