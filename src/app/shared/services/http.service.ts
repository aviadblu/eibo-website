
import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserService} from "./user.service";
import {Http, Headers, RequestOptionsArgs} from "@angular/http";

import {MatSnackBar} from "@angular/material";
import {HttpHeaders} from "@angular/common/http";
import {catchError, map} from "rxjs/internal/operators";

@Injectable()
export class HttpService {
  private ctx = {
    userId: 0,
    admin: false
  };

  private headers;

  constructor(private angularHttp: Http,
              private snackBar: MatSnackBar) {
    UserService.userData$.subscribe(userData => {
      if (userData.uid != null) {
        this.ctx.userId = userData.id;
        this.ctx.admin = userData.admin || false;
        this.createCtxHeader();
      }
    });
  }

  private createCtxHeader() {
    this.headers = new Headers();
    this.headers.append('Ctx', btoa(JSON.stringify(this.ctx)));
  }

  private createOptions(options) {
    if (!options) {
      return {
        headers: this.headers
      }
    } else if (!options.headers) {
      options.headers = this.headers;
      return options;
    } else {
      const keys = this.headers.keys();
      let i = 0;
      this.headers.forEach(val => {
        options.headers.append(keys[i], val[i]);
        i++;
      });
      return options;
    }
  }

  private handleResponseError(error) {
    let message = error.toString();
    if (error._body) {
      const b = JSON.parse(error._body);
      message = b.message || b.detail;
    }
    this.snackBar.open(`Error ${error.status} ${error.statusText} => Details: ${message}`, "close");
    return observableThrowError(message);
  }

  get (url: string, options?: RequestOptionsArgs): Observable<any> {
    return this.angularHttp.get(url, this.createOptions(options))
      .pipe(map(res => res.json()),
      catchError(err => {
        return this.handleResponseError(err);
      }));
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    return this.angularHttp.post(url, body, this.createOptions(options))
      .pipe(map(res => res.json()),
      catchError(err => {
        return this.handleResponseError(err);
      }));
  }


  put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    return this.angularHttp.put(url, body, this.createOptions(options))
      .pipe(map(res => res.json()),
      catchError(err => {
        return this.handleResponseError(err);
      }));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    return this.angularHttp.delete(url, this.createOptions(options))
      .pipe(map(res => res.json()),
      catchError(err => {
        return this.handleResponseError(err);
      }));
  }

}
