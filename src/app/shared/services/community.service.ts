import {BehaviorSubject, Observable} from "rxjs";
import { Injectable } from "@angular/core";
import { CloudinaryHelperService } from "./cloudinary-helper.service";
import { UserService } from "./user.service";
import { HttpService } from "./http.service";
import {map, take} from "rxjs/internal/operators";
import {isNullOrUndefined} from "util";
import {User} from "../../client/types/user.module";

export interface SearchResultCommunity {
  id: number;
  name: string;
  address: any;
  logo: string;
}

export class Community {
  id?: number;
  name: string;
  address?: any;
  logo?: string;
  owner_id?: string | number;
  joined?: boolean;
  usercommunityid: number;
  members_count: number;
  members: User[];
  description: string;
  is_approved: boolean;

  constructor(id: number, name: string, descr: string, address: any, logo: string, owner_id?: string | number, usercommunityid?: number) {
    this.id = id;
    this.name = name;
    this.description = descr;
    this.address = address;
    this.logo = logo;
    this.owner_id = owner_id;
    this.usercommunityid = usercommunityid;
  }
}

export interface UserCommunityResponse {
  id?: number;
  community_id?: number;
  user_id: number;
  name: string;
  address?: any;
  logo?: string;
  owner_id: boolean;
}

@Injectable()
export class CommunityService {
  public userCommunities: BehaviorSubject<any>;

  constructor(private http: HttpService, private userSvc: UserService) {
    this.userCommunities = new BehaviorSubject(null);
    this.userSvc.userLoggedIn$.subscribe(this.loadUserCommunities.bind(this));
  }

  loadUserCommunities(user): void {
    if(user == null || this.userCommunities.getValue() != null)
      return;

    this.http
      .get(`/api/userCommunities/${user.id}`)
      .pipe(take(1))
      .pipe(map(res => res))
      .subscribe(res => {
        this.userCommunities.next(res);
      });
  }

  getCommunityAutoComplete(term: string): Observable<any> {
    return this.http.get("api/communities?filter=" + term).pipe(take(1))
      .pipe(map(res => {
        res["data"].map(com => {
          com.logo = CloudinaryHelperService.getUrlWithImageResize(com.logo, 92, 140);
        });
        return res["data"] as SearchResultCommunity[];
      }));
  }

  editCommunity(payload: Community, id: number): Observable<any> {
    return this.http
      .put(`/api/communities/${id}`, payload)
      .pipe(take(1))
      .pipe(map(res => res as UserCommunityResponse));
  }

  getCommunity(id: number): Observable<any> {
    return this.http
      .get(`/api/communities/${id}`)
      .pipe(take(1))
      .pipe(map(res => res as UserCommunityResponse));
  }
  getAll(): Observable<any> {
    return this.http
      .get(`/api/communities/`)
      .pipe(take(1))
      .pipe(map(res => res.data as UserCommunityResponse));
  }
  addCommunity(payload: Community): Observable<any> {
    return this.http
      .post(`/api/communities`, payload)
      .pipe(take(1))
      .pipe(map(res => res as UserCommunityResponse));
  }

  leaveCommunity(communityUserId :number): Observable<any> {
    return this.http
      .delete(`/api/userCommunities/${communityUserId}`)
      .pipe(take(1))
      .pipe(map(res => res as UserCommunityResponse))
      .pipe(map(res => {
        let idxToRemove = this.userCommunities.getValue()
          .findIndex((uc) => uc.usercommunityid == communityUserId);
        this.userCommunities.getValue().splice(idxToRemove, 1);
        return res;
      }));
  }

  joinCommunityByUserCommunity(uc: UserCommunityResponse): Observable<any> {
    let community = new Community(uc.community_id, uc.name, null, uc.address, uc.logo);
    return this.joinCommunity(community);
  }

  joinCommunity(community: Community): Observable<any> {
    return this.http
      .post(`/api/userCommunities/${this.userSvc.user.id}`, {
        communityId: community.id
      })
      .pipe(take(1))
      .pipe(map(res => res as UserCommunityResponse))
      .pipe(map(res => {
        community.usercommunityid = res.id;
        this.userCommunities.getValue().push(community);
        return res;
      }));
  }

  deleteCommunity(community) {
    return this.http
      .delete(`/api/communities/${community.id}`)
      .pipe(take(1))
      .pipe(map(res => res));
  }

  profileUserCommunities(uid: string | number): Observable<any> {
    return this.http
      .get(`/api/userCommunities/${uid}`)
      .pipe(take(1))
      .pipe(map(res => res));
  }

  joinOrRemoveUserCommunity(community: Community) {
    if(!community.joined) {
      this.joinCommunity(community).subscribe(() => {
        community.joined = true;
      });
    } else {
      this.leaveCommunity(community.usercommunityid).subscribe(() => {
        community.joined = false;
      });
    }
  }

  updateCommunitiesJoinedStatus(userCommunities: Community[], communities: any[]) {
    if (communities && userCommunities) {
      communities.forEach((c) => {
        let uc = userCommunities.find((uc) => uc.id == c.id);
        if (!isNullOrUndefined(uc)) {
          c.joined = true;
          c.usercommunityid = uc.usercommunityid;
        }
      })
    }
  }

  updateCommunity(community: Community) {
    return this.http
      .put(`/api/communities/${community.id}`, community)
      .pipe(take(1))
      .pipe(map(res => res as Community));
  }
}
