import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {UserService} from "./user.service";
import {HttpService} from "./http.service";
import {MyLibraryService} from "./my-library.service";
import {map, take} from "rxjs/internal/operators";
import {Book} from "../models/book";

@Injectable()
export class UserBookService {
  public userChildrenBooks: BehaviorSubject<any> = new BehaviorSubject(null);
  public userReadingBooks: BehaviorSubject<any> = new BehaviorSubject(null);
  public userNonfictionBooks: BehaviorSubject<any> = new BehaviorSubject(null);
  public userLearningBooks: BehaviorSubject<any> = new BehaviorSubject(null);

  public static CHILDREN_CATEGORY = 'ילדים ונוער';
  public static READING_CATEGORY = 'ספרי קריאה';
  public static NONFICTION_CATEGORY = 'עיון';
  public static LEARNING_CATEGORY = 'ספרי לימוד';
  public static TRANSLATE_CATEGORIES = {
    en: {
      [UserBookService.CHILDREN_CATEGORY]: 'Children Books',
      [UserBookService.READING_CATEGORY]: 'Reading Books',
      [UserBookService.NONFICTION_CATEGORY]: 'Nonfiction Books',
      [UserBookService.LEARNING_CATEGORY]: 'Learning Books'
    }
  };

  constructor(private userSvc: UserService,
              private http: HttpService) {
  }

  loadUserBooks(): void {
    let self = this;
    this.http.get(`/api/userBooks/${this.userSvc.user.id}`)
      .pipe(take(1))
      .subscribe((books :Book[]) => {
        books = books.map(MyLibraryService.mapBookCoverUrl);
        self.userChildrenBooks.next(self.getBooksByCategory(books, UserBookService.CHILDREN_CATEGORY));
        self.userReadingBooks.next(self.getBooksByCategory(books, UserBookService.READING_CATEGORY));
        self.userNonfictionBooks.next(self.getBooksByCategory(books, UserBookService.NONFICTION_CATEGORY));
        self.userLearningBooks.next(self.getBooksByCategory(books, UserBookService.LEARNING_CATEGORY));
      });
  }

  private getBooksByCategory(books: Book[], category :string) {
    return books ? books.filter((b) => b.category == category) : null;
  }

  getIfHasBooks() {
    return this.http.get(`/api/userBooks/${this.userSvc.user.id}`)
      .pipe(take(1))
  }

  profileUserBooks(uid: string): Observable<any> {
    return this.http.get(`/api/userBooks/${uid}`)
      .pipe(take(1));
  }


  getUserBook(userBookId): Observable<any> {
    return this.http.get(`/api/userBooks/${userBookId}/book`)
      .pipe(take(1)).pipe(map((res) => MyLibraryService.mapBookCover(res[0])));
  }

  editUserBook(userBookId, payload): Observable<any> {
    return this.http.put(`/api/userBooks/${userBookId}`, payload)
      .pipe(take(1));
  }

  addUserBook(payload): Observable<any> {
    return this.http.post(`/api/userBooks/${this.userSvc.user.id}`, payload)
      .pipe(take(1));
  }

  deleteUserBook(bookUserid: number) {
    return this.http.delete(`/api/userBooks/${bookUserid}`)
      .pipe(take(1));
  }
}
