import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {FirebaseService} from "./firebase.service";
import {FacebookService as FBService, InitParams} from 'ngx-facebook';

export const STATUS_NOT_YET_DEFINED = 'not yet defined';

@Injectable()
export class FacebookService {
  isLoggedIn$: BehaviorSubject<any> = new BehaviorSubject<any>(STATUS_NOT_YET_DEFINED);
  userData = {};

  constructor(private FB: FBService) {

    const params: InitParams = {
      appId: '809953752496273',
      xfbml: true,
      version: 'v2.11'
    };

    const self = this;
    this.FB.init(params);

    FB.getLoginStatus()
      .then((response) => {
      if (self.isConnected(response)) {
        self.setUserParams(response);
      } else {
        self.isLoggedIn$.next(self.isConnected(response));
      }
    });
  }

  private setUserParams(response) {
    const self = this;
    const accessToken = response.authResponse["accessToken"];
    this.FB.api('/me', 'get',{fields: "id,name,email,picture.width(200)"})
      .then(function (response) {
      self.userData = {
        uid: FirebaseService.hashString(response['email']),
        auth_uid: "",
        data: {
          displayName: response["name"],
          email: response['email'],
          photoURL: response["picture"]["data"]["url"]
        },
        fbId: response['id'],
        fbAccessToken: accessToken
      };
      self.isLoggedIn$.next(true);
    });
  }

  login() {
    return new Promise(resolve => {
      this.FB.login({scope: 'public_profile,email,user_friends'})
        .then((response) => {
        if (this.isConnected(response)) {
          this.setUserParams(response);
        }
        resolve(this.isConnected(response))
      });
    });
  }

  private isConnected(response) {
    return response.status == "connected";
  }

  logout() {
    return new Promise(resolve => {
      this.FB.logout().then(response => {
        this.userData = {data: {}};
        this.isLoggedIn$.next(false);
        resolve(false);
      });
    });

  }

  shareApp() {
    this.FB.ui({
      method: 'share',
      href: window.location.origin
    })
  }

}
