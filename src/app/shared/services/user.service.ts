import { Injectable } from "@angular/core";
import { FirebaseService } from "./firebase.service";
import { CloudinaryHelperService } from "./cloudinary-helper.service";
import { HttpService } from "app/shared/services/http.service";
import { FacebookService } from "./facebook.service";
import {BehaviorSubject} from "rxjs/index";
import {skip, take} from "rxjs/internal/operators";
import {User} from "../../client/types/user.module";

@Injectable()
export class UserService {
  public auth$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public user$: BehaviorSubject<any> = new BehaviorSubject(null);
  public userLoggedIn$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  public tutorial = { nav: false, home: false };
  public app_options$: BehaviorSubject<any> = new BehaviorSubject({
    'profileHideTutorialSwap':false,
    'profileHideTutorialChat': true,
    'profileWizardSkipped': true
  });

  public static userData$: BehaviorSubject<any> = new BehaviorSubject({
    uid: null
  });
  user = null;

  constructor(
    private firebaseSvc: FirebaseService,
    private fbSvc: FacebookService,
    private http: HttpService
  ) {
    // listen to logged in status, and if logged in:
    // load from server to check if user already exists
    // update or create user in database after
    this.fbSvc.isLoggedIn$.pipe(skip(1)).subscribe(isLoggedIn => {
      this.auth$.next(isLoggedIn);
      if (isLoggedIn) {
        this.loadUser(this.getFacebookUserEmail()).then(
          (user :User) => {
            this.userLoggedIn$.next(user);
            this.saveUser(user);
          }
        );
      }
    });
  }

  private saveUser(user) {
    if (user) {
      return this.updateUser(user);
    } else {
      return this.createUser();
    }
  }

  private createUser() {
    return this.http
      .post("/api/users", this.fbSvc.userData)
      .pipe(take(1))
      .subscribe(res => {
        this.loadUser(this.getFacebookUserEmail())
          .then((user :User) => this.userLoggedIn$.next(user));
      });
  }

  private getFacebookUserEmail() {
    return this.fbSvc.userData["data"]["email"];
  }

  private updateUser(user) {
    return this.http
      .put(`/api/users/${user.id}`, this.fbSvc.userData)
      .pipe(take(1))
      .subscribe(res => {
        this.loadUser(this.getFacebookUserEmail());
      });
  }

  private loadUser(email) {
    return new Promise((resolve, reject) => {
      const uid = FirebaseService.hashString(email);
      this.http
        .get(`api/users/${uid}`)
        .pipe(take(1))
        .subscribe(data => {
          if (data.error) {
            resolve();
          } else {
            this.user = data;
            UserService.userData$.next(data);
            this.user.thumbnail = CloudinaryHelperService.getUrlWithImageResize(
              this.user.photo,
              100,
              100
            );
            this.user.thumbnailSmall = CloudinaryHelperService.getUrlWithImageResize(
              this.user.photo,
              40,
              40
            );
            this.user$.next(this.user);
            resolve(data);
          }
        });
    });
  }

  public loadUserData(uid) {
    return this.http.get(`api/users/${uid}`).pipe(take(1));
  }

  public loadUserDataPromise(uid) {
    return new Promise((resolve, reject) => {
      this.http
        .get(`api/users/${uid}`)
        .pipe(take(1))
        .subscribe(resolve, reject);
    });
  }

  loginWithFacebook() {
    this.fbSvc.login();
  }


  logout() {
    return this.fbSvc.logout().then(() => {
      this.user = null;
      UserService.userData$.next({ uid: 0 });
      this.user$.next(null);
      this.auth$.next(false);
    });
  }

  updateUserAddress(address) {
    return new Promise((resolve, reject) => {
      this.http
        .put(`/api/users/updateAddress/${this.user.uid}`, address)
        .pipe(take(1))
        .subscribe(result => {
          this.userLoggedIn$.getValue().address = result.address;
          resolve(result);
        }, reject);
    });
  }

  updateApplicationOptions(option, value) {
    return new Promise((resolve, reject) => {
      this.http
        .put(`/api/users/updateApplicationOptions/${this.user.uid}`, {
          option: option,
          value: value
        })
        .pipe(take(1))
        .subscribe(result => {
          this.app_options$.next(result.app_options);
          resolve(result);
        }, reject);
    });
  }

  getUserFriends(callback) {
    this.http.get('/api/users/get/friends')
      .pipe(take(1))
      .subscribe(callback)
  }
}
