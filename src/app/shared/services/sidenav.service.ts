import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class SidenavService {
  public open$: BehaviorSubject<boolean>;
  private _isOpen: boolean = false;

  constructor() {
    this.open$ = new BehaviorSubject(this._isOpen);
  }

  toggle() {
    this._isOpen = !this._isOpen;
    this.open$.next(this._isOpen);
  }

  closedByEvent() {
    this._isOpen = false;
  }

  close() {
    this._isOpen = false;
    this.open$.next(this._isOpen);
  }
}
