import {Injectable} from '@angular/core';
import {Headers} from "@angular/http";

import {HttpService} from "./http.service";
import {take} from "rxjs/internal/operators";

@Injectable()
export class ContactService {
  private headers = new Headers();

  constructor(private http: HttpService) {
    this.headers.append('Content-Type', 'application/json');
  }

  sendContact(data: string): Promise<string> {
    return new Promise(resolve => {
      this.http.post('api/contact', JSON.stringify(data), {headers: this.headers})
        .pipe(take(1))
        .subscribe(resolve, this.handleError);
    });
  }

  sendReport(data: string): Promise<string> {
    return new Promise(resolve => {
      this.http.post('api/contact/report', JSON.stringify(data), {headers: this.headers})
        .pipe(take(1))
        .subscribe(resolve, this.handleError);
    });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
