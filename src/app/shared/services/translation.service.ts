import {Injectable} from '@angular/core';
import {LangService} from "./lang.service";
import {distinctUntilChanged} from "rxjs/internal/operators";


@Injectable()
export class TranslationService {
  private lang: string;
  private map = {};

  constructor(private langSvc: LangService) {
    langSvc.lang$.pipe(distinctUntilChanged()).subscribe(lang => {
      this.lang = lang;
    });
  }

  translate(key: string) {
    if (this.map[this.lang][key]) {
      return this.map[this.lang][key];
    } else if (this.map['en'][key]) {
      return this.map['en'][key];
    } else {
      return 'No translation found!';
    }
  }

  initViewTranslation(view, maps) {
    let flatKey;
    maps.forEach(map => {
      let currMap;
      let mapClone = JSON.parse(JSON.stringify(map));
      if (!this.map[mapClone.lang]) {
        this.map[mapClone.lang] = {};
      }
      currMap = this.map[mapClone.lang];
      delete mapClone.lang;
      Object.keys(mapClone).forEach(key => {
        flatKey = `${view}.${key}`;
        currMap[flatKey] = mapClone[key];
      });
    });
  }

}
