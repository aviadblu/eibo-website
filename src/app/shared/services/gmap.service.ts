import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {UserService} from "./user.service";
import {User} from "../../client/types/user.module";
declare var google;

@Injectable()
export class GmapService {

  constructor(private userSvc: UserService) {
  }

  createAutoCompleteField(ele, bounds?): Ac {
    return new Ac(ele, bounds);

  }


  updateUsersDistanceToUser(dataWithUserLists: User[]) {
    this.getCurrentUserLocation((userLocation) => {
      this.computeUsersDistances(userLocation, dataWithUserLists);
    });
  }

  getCurrentUserLocation(callback) {
    let user = this.userSvc.userLoggedIn$.getValue();
    let userLatLng;

    if (user && this.addressExists(user.address)) {
      userLatLng = new google.maps.LatLng(user.address.lat, user.address.lng);
      callback(userLatLng)
    } else {
      navigator.geolocation.getCurrentPosition(function (location) {
        userLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
        callback(userLatLng)
      });
    }
  }

  computeUsersDistances(userLatLng, usersList : User[]) {
    let self = this;

    usersList.forEach((user : User) => {
        if (self.addressExists(user.address)) {
          user.distance = google.maps.geometry.spherical.computeDistanceBetween(
            new google.maps.LatLng(user.address.lat, user.address.lng), userLatLng);
          user.distance = user.distance / 1000; // KM
          user.distance = Math.round(user.distance * 10) / 10; //ROUND .1
        }
    })
  }

  private addressExists(user_address: any) {
    return user_address && user_address.lat && user_address.lng;
  }
}


class Ac {
  private _autocomplete;
  public address$: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(nativeElement, bounds?) {
    let self = this;
    this._autocomplete = new google.maps.places.Autocomplete(nativeElement, {
      bounds: bounds,types: ['geocode']});
    this._autocomplete.addListener('place_changed', function () {
      let place = self._autocomplete.getPlace();
      if (!place.geometry) {
        console.log("Error: no geometry");
        return;
      } else {
        self.address$.next({
          formatted_address: place.formatted_address,
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng()
        });
      }
    });
  }


  get autocomplete() {
    return this._autocomplete;
  }

}
