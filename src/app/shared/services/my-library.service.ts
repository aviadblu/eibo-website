import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SearchResultBook } from "./../models/search-result-book";
import { CloudinaryHelperService } from "./cloudinary-helper.service";
import { Book } from "./../models/book";
import { UserService } from "./user.service";
import { HttpService } from "./http.service";
import {map, take} from "rxjs/internal/operators";

@Injectable()
export class MyLibraryService {
  constructor(private http: HttpService, private userSvc: UserService) {}

  getBooksAutocomplete(term: string, communityId? :number): Observable<any> {
    let params = `?filter=${term}`;
    if(communityId) {
      params += `&communityId=${communityId}`;
    }

    return this.http.get(`api/books/booksListNames${params}`)
      .pipe(take(1)).pipe(map(res => {
      res["data"].map(book => {
        book.cover_url = book.picture.indexOf("res.cloudinary.com") > 0
          ? CloudinaryHelperService.getUrlWithImageResize(book.picture, 92, 140)
          : book.picture;
      });
      return res["data"] as SearchResultBook[];
    }));
  }

  getUsersAutocomplete(term: string): Observable<any> {
    return this.http.get(`api/users/usersListNames?filter=${term}`).pipe(take(1)).pipe(map(res => {
      res["data"].map(MyLibraryService.mapBookCoverUrl);
      return res["data"];
    }));
  }

  static mapBookCoverUrl(book :Book) :Book {
    book.picture = book.picture || book.cover_url;
    if(book.picture) {
    book.cover_url = book.picture.indexOf("res.cloudinary.com") > 0
      ? CloudinaryHelperService.getUrlWithImageResize(book.picture, 92, 140)
      : book.picture;
    }
    return book;
  }

  editBook(payload: Book, id: number): Observable<any> {
    return this.http
      .put(`/api/books/${id}`, payload)
      .pipe(take(1))
      .pipe(map(res => res));
  }

  getBook(id: number): Observable<any> {
    return this.http.get(`/api/books/${id}`).pipe(take(1));
  }

  getBookByBarcode(barcode: string): Observable<any> {
    return this.http.get(`/api/books/getBookByBarcode/${barcode}`).pipe(take(1))
      .pipe(map(MyLibraryService.mapBookCover));
  }

  static mapBookCover(book) {
    book.cover_url = book.picture || book.cover_url;
    return book;
  }

  getBookReviews(bookId): Observable<any> {
    return this.http.get(`/api/books/${bookId}/reviews`).pipe(take(1));
  }

  addBook(payload: Book): Observable<any> {
    payload["insert_by"] = `${this.userSvc.user.name} (${
      this.userSvc.user.email
    })`;
    payload["pending"] = true;
    return this.http
      .post(`/api/books`, payload)
      .pipe(take(1))
      .pipe(map(res => res));
  }

  deleteBook(bookId) {
    return Promise.resolve({
      message: "deleted successfully"
    });
  }

  followBook(book: any) {
    return this.http.post('/api/books/follow', {bookId: book.id}).pipe(take(1));
  }

  unfollowBook(book: any) {
    return this.http.post('/api/books/unfollow', {bookId: book.id}).pipe(take(1));
  }

  isBookFollowed(book: any) {
    return this.http.post('/api/books/is-followed', {bookId: book.id}).pipe(take(1));
  }

  getUserFollowedBooks() {
    return this.http.get('/api/books/get/followed').pipe(take(1));
  }
}
