import {Injectable} from '@angular/core';

@Injectable()
export class CloudinaryHelperService {

  public static options = {cloudName: 'db5v7kkij', uploadPreset: 'isr2kwlh'};

  constructor() {
  }

  public static getUrlWithImageResize(url, width, height, resizeMode = 'c_fill') {
    if (!url || url.indexOf("https://res.cloudinary.com") == -1) {
      return url;
    }
    let splittedUrl = url.split('/upload/');
    let resizeStr = `${resizeMode},h_${height},w_${width}`;

    let newUrlArr = [
      splittedUrl[0],
      'upload',
      resizeStr,
      splittedUrl[1]
    ];

    return newUrlArr.join('/');
  };

}
