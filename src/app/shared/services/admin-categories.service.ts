import {Injectable} from '@angular/core';
import {BehaviorSubject, merge, Observable} from "rxjs";
import {DataSource} from "@angular/cdk/collections";
import {HttpService} from "./http.service";
import {map, take} from "rxjs/internal/operators";

@Injectable()
export class AdminCategoriesService {
  categories$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private adminCategoriesDataSource;

  constructor(private http: HttpService) {
    this.adminCategoriesDataSource = new AdminCategoriesDataSource(this.categories$);
  }

  init() {
    return Promise.all([
      this.loadCategoriesList()
    ]);
  }

  private loadCategoriesList() {
    return new Promise(resolve => {
      this.http.get('/api/categories')
        .pipe(take(1))
        .subscribe(categories => {
          this.categories$.next(categories);
          resolve();
        })
    });
  }

  loadCategoryData(categoryId) {
    return new Promise(resolve => {
      this.http.get(`/api/categories/${categoryId}`)
        .pipe(take(1))
        .subscribe(category => {
          resolve(category);
        });
    });

  }

  addCategory(payload) {
    return new Promise(resolve => {
      this.http.post(`/api/categories`, payload)
        .pipe(take(1))
        .subscribe(category => {
          this.loadCategoriesList()
            .then(() => {
              resolve(category);
            });
        });
    });
  }

  updateCategory(categoryId, payload) {
    return new Promise(resolve => {
      this.http.put(`/api/categories/${categoryId}`, payload)
        .pipe(take(1))
        .subscribe(category => {
          this.loadCategoriesList()
            .then(() => {
              resolve(category);
            });
        });
    });
  }

  deleteCategory(categoryId) {
    return new Promise(resolve => {
      this.http.delete(`/api/categories/${categoryId}`)
        .pipe(take(1))
        .subscribe(() => {
          this.loadCategoriesList().then(resolve);
        });
    });
  }

  getAdminCategoriesDataSource() {
    return this.adminCategoriesDataSource;
  }

}

class AdminCategoriesDataSource extends DataSource<any> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  constructor(private categories$) {
    super();
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.categories$,
      this._filterChange,
    ];


    return merge(...displayDataChanges).pipe(map(() => {
      let categories = [];
      this.categories$.pipe(take(1)).subscribe(categoriesRes => {
        categories = categoriesRes;
      });
      return categories.filter(category => {
        let searchStr = category.name.toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) != -1;
      });
    }));
  }

  disconnect() {
  }

}

