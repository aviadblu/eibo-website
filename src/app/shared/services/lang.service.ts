import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable()
export class LangService {
  private _lang = 'en';
  private optionalLangs = ['en','he'];
  public lang$: BehaviorSubject<string>;

  constructor() {
    // browser lang disabled until we finish hebrew translation and RTL support
    // if (localStorage.getItem('_lang')) {
    //   this._lang = localStorage.getItem('_lang');
    // } else if(navigator.language) {
    //   this.optionalLangs.forEach(lang => {
    //     if(navigator.language.indexOf(lang) > -1) {
    //       this._lang = lang;
    //     }
    //   });
    // }
    this.lang$ = new BehaviorSubject(this._lang);
  }

  set lang(value: string) {
    this._lang = value;
    localStorage.setItem('_lang', value);
    this.lang$.next(this._lang);
  }
}
