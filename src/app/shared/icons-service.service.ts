import { Injectable } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {MatIconRegistry} from "@angular/material";

@Injectable()
export class IconsService {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer) {

  }

  initSvgIcons() {

    this.matIconRegistry.addSvgIcon(
      "barcode",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/barcode.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "books-stack",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/books-stack.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "speech-bubble",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/speech-bubble.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "magnifier",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/magnifier.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "whatsapp-logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/whatsapp-logo.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "facebook-logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/img/icons/facebook-logo.svg")
    );
  }
}
