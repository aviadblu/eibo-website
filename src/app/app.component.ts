import {Component, HostListener} from '@angular/core';
import {UserService} from "./shared/services/user.service";
import {FacebookService} from "./shared/services/facebook.service";
import {MatDialog, MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {IconsService} from "./shared/icons-service.service";
import {InfoDialogComponent} from "./shared/components/info-dialog/info-dialog.component";
import {Observable, Subject} from "rxjs/Rx";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'eibo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'eibo works!';
  ready = false;

  constructor(private userSvc: UserService,
              private fbSvc: FacebookService,
              private dialog: MatDialog,
              private iconService: IconsService) {
    this.iconService.initSvgIcons();

    this.userSvc.user$.subscribe(user => {
       //if (user) {

        this.ready = true;
      //}
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  preventUnsavedChanges($event) {
    return false;
  }
}
