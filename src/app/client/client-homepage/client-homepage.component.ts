import { Component, ElementRef, OnInit, AfterViewInit } from '@angular/core';

import { TranslationService } from '../../shared/services/translation.service';
import { UserService } from '../../shared/services/user.service';
import { langMaps } from './homepage.text';
import {FacebookService, STATUS_NOT_YET_DEFINED} from "../../shared/services/facebook.service";
import {Suggestion} from "../types/suggestion.module";
import {GeneralService} from "../../shared/services/general.service";
import {Book} from "../../shared/models/book";
import {ActivatedRoute, Router} from "@angular/router";
import {take} from "rxjs/internal/operators";

@Component({
  selector: "eibo-client-homepage",
  templateUrl: "./client-homepage.component.html",
  styleUrls: ["./client-homepage.component.less"]
})
export class ClientHomepageComponent implements OnInit, AfterViewInit {
  user = null;
  public userExistsStatus: any = STATUS_NOT_YET_DEFINED;

  showTutorial = false;
  public isVideoPlaying = false;
  video_id = "promo_video/VID-20171117-WA0001";
  state;

  suggestedBooks: Book[] = [];
  private lastBooksArraySize: number = 0;

  constructor(private translationSvc: TranslationService,
              private userSvc: UserService,
              private fbSvc: FacebookService,
              private elRef: ElementRef,
              private genralService: GeneralService,
              private router: Router,
              private route: ActivatedRoute) {
    translationSvc.initViewTranslation("homepage", langMaps);
    this.loadSuggestedBooksWhenUserLogsIn();
  }

  private loadSuggestedBooksWhenUserLogsIn() {
    this.genralService.userSuggestions$.subscribe((suggestions: Suggestion[]) => {
      suggestions ? suggestions.forEach((s) => {
        let rating = s.reviewCount > 0 ? (s.reviewScore / s.reviewCount) : 0;
        this.suggestedBooks.push(
          new Book(parseInt(s.bookId), s.bookName, s.bookAuthor, s.bookCover, s.bookCover, rating));
      }) : null;
    });

    this.userSvc.userLoggedIn$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.genralService.loadUserSuggestions(user.id);
      }
    })
  }

  videoPlay() {
    this.elRef.nativeElement.querySelector(`cl-video video`).play();
    this.isVideoPlaying = true;
  }

  videoPause() {
    this.elRef.nativeElement.querySelector(`cl-video video`).pause();
    this.isVideoPlaying = false;
  }

  fbLogin() {
    this.state = 'loading';
    this.userSvc.loginWithFacebook();
  }

  ngAfterViewInit() {

  }
  ngOnInit() {
    this.userSvc.auth$.subscribe(auth => {
      if (!auth) {
        this.state = 'notauth';
      } else {
        this.state = 'auth';
      }
    });

    this.fbSvc.isLoggedIn$.subscribe((status :boolean) => {
      this.userExistsStatus = status;
      this.route.queryParams.pipe(take(1)).subscribe((params) => {
        if(params.redirect) {
          this.router.navigate([params.redirect]);
        }
      })
    });
  }
  translate(key) {
    return this.translationSvc.translate(key);
  }

  userExists() {
    return this.userExistsStatus === true;
  }

  userDoesNotExist() {
    return this.userExistsStatus === false;
  }

  navigateToBook(book) {
    this.router.navigate(['/app/book/', book.id])
  }

  loadMoreBooks(lastBooksArraySize :number) {
    if(lastBooksArraySize > this.lastBooksArraySize) {
      this.lastBooksArraySize = lastBooksArraySize;
      this.genralService.loadUserSuggestions(this.user.id, this.suggestedBooks.map((b) => b.id))
    }
  }
}
