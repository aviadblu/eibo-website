const en = {
  lang: 'en',
  "intro":`
  EiBO is a social impact cross-platform app that is developing book-sharing communities and changing the culture of real book consumption, while empowering local neighbourhood economies
  `,
  "intro2Why": `About Us`,
  "intro2Content": `
    EIBO is a novel, first-of-its-kind type of social library. It allows people to borrow and exchange
    books among themselves and within various reader communities. EIBO helps you find any book
    you’re looking for within walking distance – and it’s free!<br><br>
    The ties we develop with our close environment, with the people around us, and with the
    communities to which we belong, enable us to change our consumption choices. These changes
    make our lives simpler, more comfortable, eco-friendly, and also cost-effective. EIBO is a novel
    platform that lets us find any book we desire in a simpler, easier, and more economical way. By
    relying on other users who are already in our spheres of interaction (either residential, work-
    related, or social), it allows us to conduct an efficient search 
  `,
  "intro2ReadMore": `Read More`,
  "buildYourLibrary": `Build your library and make the world a better place...`,
  "startNow": 'Start Now'
};

const he = {
  lang: 'he',
  "intro": `
              איבו היא ספריה חברתית ראשונה מסוגה המאפשרת החלפה והשאלה של ספרים בין אנשים ובתוך קהילות קוראים . איבו תאפשר
            לכם למצוא כל ספר במרחק הליכה ובחינם.
  `,
  "intro2Why": `למה?`,
  "intro2Content": `החיבור לסביבה הקרובה שלנו, לאנשים שמסביבנו, ולקהילות אליהן אנחנו שייכים ובהן אנו פועלים, מאפשרים לנו לעשות
              שינויים בתרבות הצריכה שלנו. שינויים שהופכים את החיים שלנו לפשוטים יותר, נוחים יותר, ירוקים יותר וחסכוניים
              יותר. איבו היא פלטפורמה חדשנית המאפשרת לנו למצוא כל ספר בדרך פשוטה נוחה וחסכונית יותר דרך משתמשים אחרים
              הנמצאים בסביבת העבודה, המגורים, או קהילת החברים אליה אנו שייכים בכל מקום בעולם.`,
  "intro2ReadMore": `קרא עוד`,
  "buildYourLibrary": `בנה את הספרייה שלך והפוך את העולם למקום טוב יותר...`,
  "startNow": 'התחל עכשיו'
};

export const langMaps = [en,he];
