import { DialogDeleteComponent } from '../../shared/components/dialog-delete/dialog-delete.component';
import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {ChatsService} from "../../shared/services/chats.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";


import { UserBookService } from 'app/shared/services/user-book.service';
import {MatDialog} from '@angular/material';
import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core';
import { UserService } from '../../shared/services/user.service';

@Component({
  selector: 'eibo-client-chat',
  templateUrl: './client-chat.component.html',
  styleUrls: ['./client-chat.component.less']
})
export class ClientChatComponent implements OnInit,OnDestroy, AfterViewInit {
  public chatMetaData;
  public newMessageForm: FormGroup;
  private chatId: string = null;
  public messages$;
  public chats$;
  public chatSubscription: Subscription;
  public userBooks = {
    id:0,
    data:[]
  };
  public messageIsHebrew:boolean;
  public showUserBooks:boolean;

  showTutorial=false;
  emojiMartShow = false;

  SWIPE_ACTION = { LEFT: "swipeleft", RIGHT: "swiperight" };

  public sliderOffset=0;

  @ViewChild('chatPane') private chatPane:ElementRef;
  @ViewChild('input') private input:ElementRef;
  @ViewChild('emojis') emojis:any;

  constructor(private route: ActivatedRoute,
              private chatSvc: ChatsService,
              private usrBookSvc: UserBookService,
              private usrSvc: UserService,
              private dialog: MatDialog,
              private fb: FormBuilder) {
    this.messages$ = this.chatSvc.chat$;
    this.chats$ = this.chatSvc.chats$;
    this.createForm();
  }

  swap(targetUserBook,targetUser) {
    let dialog = this.dialog.open(DialogDeleteComponent, {
      data: {caption: 'Do you want to swap ' + targetUserBook.name + '?'}
    });
    dialog.afterClosed().subscribe((confirm) => {
      if (confirm) {
        this.chatSvc.checkCreateNewChat(targetUser.targetId)
          .then((chatId:string) => {

            this.chatSvc.bookSwap(chatId,
              {
                id: targetUser.targetId,
                name: targetUser.targetName,
                photo: targetUser.targetPhoto,
                pg: targetUser.targetPg,
                distance: 2.5,
              },
              {
                bookId: targetUserBook.bookid,
                bookName: targetUserBook.name,
                bookAuthor: targetUserBook.author,
                bookCover: targetUserBook.picture,
                bookReview: targetUserBook.summary,
                reviewScore: targetUserBook.rating,
              },
              this.usrSvc.user.name
            ).then(()=>{
              this.scrollToBottom();
            });
          });
      }
    });
  }
  ngOnDestroy() {
    if(this.chatSubscription) this.chatSubscription.unsubscribe();
  }
  hideTutorial() {
    this.showTutorial = false;
    this.usrSvc
      .updateApplicationOptions("profileHideTutorialChat", true)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });



  }
  ngAfterViewInit() {

  }
  ngOnInit() {

    this.usrSvc.app_options$.subscribe((app_options)=>{
        if(app_options.profileHideTutorialSwap) {
          this.showTutorial = false;
        } else {
          this.showTutorial = true;
        }
    });

    this.newMessageForm.valueChanges.subscribe((data)=>{
      let AlphaNumericChars = new RegExp("^[a-zA-Z0-9\-]+$");
      if (!AlphaNumericChars.test(data['message'])) {
        this.messageIsHebrew = true;
      } else {
        this.messageIsHebrew = false;
      }
    });



    this.route.params.subscribe(params => {
      if(params.chatId) {
        this.chatId = params.chatId;
        this.chatSvc.zeroCounter(this.chatId);
        this.chatSvc.initialized$.subscribe(init => {
          if(init) {
            this.chatMetaData = this.chatSvc.getChatData(this.chatId);
            this.chatSvc.openChatConnection(this.chatId);
            this.scrollToBottom();
            this.showUserBooks = false;
          }
        });
        this.chatSubscription = this.chatSvc.chat$.subscribe(()=>{
          setTimeout(() => {
            this.scrollToBottom();
            this.chatSvc.zeroCounter(this.chatId);
          }, 1);
        });
      } else {
        this.messages$ = null;
      }


    });
  }
  toggleLibrary(userId) {
    this.sliderOffset = 0;
    this.showUserBooks = this.showUserBooks ? false : true;
    if(userId != this.userBooks.id) {
      this.userBooks['id'] = userId;
      this.userBooks['data'] = [];
      this.usrBookSvc.profileUserBooks(userId).subscribe(books=>{
        this.userBooks['data'] = books;
      });
    }
  }

  scrollToBottom(): void {
    try {
        this.chatPane.nativeElement.scrollTop = this.chatPane.nativeElement.scrollHeight;
    } catch(err) {console.log(err)}
  }

  private createForm() {
    this.newMessageForm = this.fb.group({
      message: ["", Validators.required]
    });
  }

  slideLeft() {
    this.sliderOffset = this.sliderOffset>0 ? this.sliderOffset-1 : 0;
  }

  slideRight() {
    this.sliderOffset = this.sliderOffset<Math.floor(this.userBooks.data.length/2) ? this.sliderOffset+1 :Math.floor(this.userBooks.data.length/2);
  }
  swipe(action = this.SWIPE_ACTION.RIGHT) {
    if (action === this.SWIPE_ACTION.RIGHT) {
      this.slideLeft();
    }
    if (action === this.SWIPE_ACTION.LEFT) {
      this.slideRight();
    }
  }

  onSubmitMessage() {
    if(this.newMessageForm.valid) {
      this.chatSvc.message(this.chatId, this.chatMetaData.targetId, this.newMessageForm.value)
      .then(res=>{
        this.scrollToBottom();
      });
      this.newMessageForm.reset();
      this.input.nativeElement.focus();
    }
  }

  addEmojiToInput(event) {
    let val = this.newMessageForm.get('message').value;
    this.newMessageForm.get('message').setValue(val + event.emoji.native);
  }

  hideEmojiMartShow() {
    this.emojiMartShow = false;
  }

  toggleEmojiMart() {
    this.emojiMartShow = !this.emojiMartShow;
    if(this.emojiMartShow) {
      setTimeout(this.emojis.searchRef.inputRef.nativeElement.focus(), 100);
    }
  }
}
