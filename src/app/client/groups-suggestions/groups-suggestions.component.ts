import {Component, OnDestroy, OnInit} from '@angular/core';
import {Community, CommunityService} from "../../shared/services/community.service";
import {Subscription} from "rxjs/internal/Subscription";
import {combineLatest} from "rxjs/index";

@Component({
  selector: 'eibo-groups-suggestions',
  templateUrl: './groups-suggestions.component.html',
  styleUrls: ['./groups-suggestions.component.less']
})
export class GroupsSuggestionsComponent implements OnInit, OnDestroy {

  config :SwiperOptions = {
    slidesPerView: 2.5,
    freeMode: true,
    slidesOffsetAfter: 15
  };
  communities;
  private userCommunities: Community[];
  loading :boolean = true;
  private sub: Subscription;

  constructor(private communityService : CommunityService) {
    let getAll$ = communityService.getAll();
    let getUserCommunities$ = communityService.userCommunities;

    this.sub = combineLatest(getAll$, getUserCommunities$)
      .subscribe((val) => {
      this.communities = val[0];
      this.userCommunities = val[1];
      if(this.communities != null && this.userCommunities != null){
        this.loading = false;
        this.communityService.updateCommunitiesJoinedStatus(this.userCommunities, this.communities);
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  joinOrRemoveCommunity(community :Community, event) {
    event.stopPropagation();
    this.communityService.joinOrRemoveUserCommunity(community);
  }
}
