import {Component, OnInit, OnDestroy, ViewChild, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {LangService} from "app/shared/services/lang.service";
import {SidenavService} from "../../shared/services/sidenav.service";
import {MatDialog} from "@angular/material";
import {ClientProfileWizardComponent} from "../client-profile-wizard/client-profile-wizard.component";
import {ChatsService} from "../../shared/services/chats.service";
import {UserService} from "../../shared/services/user.service";
@Component({
  selector: 'eibo-client-main',
  template: `
    <div class="matl-layout__container" [class.lang-he]="lang == 'he'" [class.sidenav-open]="sidenavOpen">
      <mat-sidenav-container class="example-container">
        <mat-sidenav [position]="lang == 'he' ? 'end' : 'start'" #sidenav class="example-sidenav">
          <eibo-client-sidenav></eibo-client-sidenav>
        </mat-sidenav>
        <eibo-client-navbar></eibo-client-navbar>
        <router-outlet></router-outlet>
      </mat-sidenav-container>
    </div>
  `,
  styles: [`
    mat-sidenav-container {
      background-color:white;
    }
    mat-sidenav-container ::ng-deep mat-sidenav-content {
      display: flex;
      -webkit-flex-direction: column;
      flex-direction: column;
    }
  `]
})
export class ClientMainComponent implements OnInit, OnDestroy, AfterViewInit {
  private sub: any[] = [];
  public lang = null;
  public sidenavOpen: boolean = false;
  public ready: boolean = false;

  @ViewChild('sidenav') sidenav;

  constructor(private route: ActivatedRoute,
              private langSvc: LangService,
              private sidenavSvc: SidenavService,
              private userSvc: UserService,
              private dialog: MatDialog,
              private chatsSvc: ChatsService) {
    this.sub.push(langSvc.lang$.subscribe(lang => {
      this.lang = lang;
    }));
  }

  ngOnInit() {
    this.sub.push(this.route.queryParams.subscribe(params => {
      if (params['lang']) {
        this.lang = params['lang'];
        this.langSvc.lang = this.lang;
      }
    }));


    this.sub.push(this.sidenavSvc.open$.subscribe(open => {
      this.sidenavOpen = open;
      if (open) {
        this.sidenav.open();
      } else {
        this.sidenav.close();
      }
    }));


  }
  ngAfterViewInit() {
    this.initProfileWizard();
  }

  private initProfileWizard() {
     this.userSvc.userLoggedIn$.subscribe(userData=>{
        if(userData && 'app_options' in userData) {
          this.userSvc.app_options$.next(userData.app_options);
          if ('profileWizardSkipped' in userData.app_options &&
            userData.app_options.profileWizardSkipped === false) {
            setTimeout(() => {
              this.dialog.open(ClientProfileWizardComponent);
            }, 1);
          }
       }
    });
  }

  ngOnDestroy() {
    this.sub.forEach(sub => {
      sub.unsubscribe();
    });
  }

}
