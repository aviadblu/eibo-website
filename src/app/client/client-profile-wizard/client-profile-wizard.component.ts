import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GmapService } from "../../shared/services/gmap.service";
import { UserBookService } from "../../shared/services/user-book.service";
import {Community, CommunityService, UserCommunityResponse} from "../../shared/services/community.service";
import { UserService } from "../../shared/services/user.service";
import { IAddress } from "../../shared/models/user";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "eibo-client-profile-wizard",
  templateUrl: "./client-profile-wizard.component.html",
  styleUrls: ["./client-profile-wizard.component.less"]
})
export class ClientProfileWizardComponent implements OnInit {
  @ViewChild("address") address;

  isLinear = true;
  addressFormGroup: FormGroup;
  libraryFormGroup: FormGroup;
  communitiesFormGroup: FormGroup;

  public summaryData = {
    address: "",
    library: [],
    communities: []
  };

  public _address: IAddress = {
    formatted_address: "",
    lat: 0,
    lng: 0
  };



  communities: Community[];
  selectedCommunity: Community;

  currentStep = 1;
  totalSteps = 4;
  @ViewChild('geolocationField') private geolocationField;

  constructor(
    private _formBuilder: FormBuilder,
    private userSvc: UserService,
    private gmapSvc: GmapService,
    private userBookSvc: UserBookService,
    private commSvc: CommunityService,
    private router: Router,
    public dialogRef: MatDialogRef<ClientProfileWizardComponent>
  ) {}

  ngOnInit() {
    let self = this;
    self.commSvc.getAll().subscribe(res => {
      self.communities = res;
      self.commSvc.userCommunities.subscribe((uc) => {
        self.commSvc.updateCommunitiesJoinedStatus(uc, self.communities);
      })
      self.selectCommunity(0);
    });
  }

  gotoLibrary(){
    this.router.navigate(["app/u/profile/library",{openAddBook:true}]);
    this.gotoStep(this.currentStep + 1);
    this.hideWizard();
  }

  saveCommunity() {
    this.commSvc.joinCommunity(this.selectedCommunity).subscribe(() => {
      this.selectedCommunity.joined = true;
    });
  }

  selectCommunity(index: number) {
    this.selectedCommunity = this.communities[index];
  }
  gotoStep(stepNumber: number) {
    this.currentStep = stepNumber;
  }

  hideWizard() {
    this.dialogRef.close();
    this.userSvc
      .updateApplicationOptions("profileWizardSkipped", true)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }

  saveAddress() {
    this.geolocationField.updateUserAddress();
  }
}
