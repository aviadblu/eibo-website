import {
  Component, ContentChild, Input, OnDestroy, OnInit, ViewChild
} from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from "rxjs/internal/operators";

@Component({
  selector: "eibo-client-search",
  templateUrl: "./client-search.component.html",
  styleUrls: ["./client-search.component.less"]
})
export class ClientSearchComponent implements OnInit, OnDestroy {
  public SearchTerm = new BehaviorSubject<string>('');
  private firstSearchResults = [];
  private secondSearchResults = [];
  public noResultsAfterSearch: boolean;
  @ViewChild('input') private input;
  private subscribers :Subscription[] = [];
  private searchesDone: number;
  @ContentChild('firstSearchTemplate') firstSearchTemplate;
  @ContentChild('secondSearchTemplate') secondSearchTemplate;

  @Input() firstObservableGetter;
  @Input() firstSearcherTitle :string;
  @Input() secondObservableGetter;
  @Input() secondSearcherTitle :string;
  @Input() placeholder :string;
  @Input() noResultsText: string;
  @Input() withSearchTitle: boolean = true;
  private numberOfObseravles: number;

  constructor() {

  }

  public search(term: string) {
    this.SearchTerm.next(term);
  }

  ngOnInit() {
    this.numberOfObseravles = 1;
    if(this.secondObservableGetter) {
      this.numberOfObseravles = 2;
    }

    this.SearchTerm.pipe(debounceTime(300),
      distinctUntilChanged())
      .subscribe(term => {
        this.searchesDone = 0;
        if (term && term.length > 1) {
          let firstSub = this.firstObservableGetter(term).subscribe(res => {
            this.firstSearchResults = res;
            this.setNoResults(res, this.secondSearchResults);
          });
          this.subscribers.push(firstSub);

          if(this.secondObservableGetter) {
            let secondSub = this.secondObservableGetter(term).subscribe(res => {
              this.secondSearchResults = res;
              this.setNoResults(res, this.firstSearchResults);
              this.subscribers.push(secondSub);
            });
          }
        } else {
          this.noResultsAfterSearch = false;
          this.firstSearchResults = [];
          this.secondSearchResults = [];
        }
      });
  }

  private setNoResults(res, otherRes) {
    this.searchesDone++;
    this.noResultsAfterSearch = (res.length === 0 && otherRes.length === 0)
      && this.searchesDone == this.numberOfObseravles;
  }

  clearText() {
    this.subscribers.forEach((s) => s.unsubscribe());
    this.search('');
    this.input.nativeElement.value = '';
    this.noResultsAfterSearch = false;
  }

  ngOnDestroy() {
    this.subscribers.forEach((s) => s.unsubscribe());
  }
}
