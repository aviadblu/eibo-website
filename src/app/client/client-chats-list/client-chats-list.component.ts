import { Component, OnInit } from '@angular/core';
import {ChatsService} from "../../shared/services/chats.service";
@Component({
  selector: 'eibo-client-chats-list',
  templateUrl: './client-chats-list.component.html',
  styleUrls: ['./client-chats-list.component.less']
})
export class ClientChatsListComponent implements OnInit {
  chats$;

  constructor(private chatsSvc: ChatsService) {
    this.chats$ = chatsSvc.chats$;
  }

  ngOnInit() {
  }

}
