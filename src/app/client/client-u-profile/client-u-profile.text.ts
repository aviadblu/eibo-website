const en = {
  lang: 'en',
  "pageHeadline": `Contact Us`,
};

const he = {
  lang: 'he',
  "pageHeadline": `צור קשר`,
};

export const langMaps = [en,he];
