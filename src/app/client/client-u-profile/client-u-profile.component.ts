import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {TranslationService} from '../../shared/services/translation.service';


import {MyLibraryService} from '../../shared/services/my-library.service';
import {langMaps} from './client-u-profile.text';
import {UserBookService} from "../../shared/services/user-book.service";
import {AddBookComponent} from "./my-library/add-book/add-book.component";

@Component({
  selector: 'eibo-client-u-profile',
  templateUrl: './client-u-profile.component.html',
  styleUrls: ['./client-u-profile.component.less']
})
export class ClientUProfileComponent implements OnInit {
  books: any;

  constructor(private translationSvc: TranslationService,
              public dialog: MatDialog,
              private mylibrary: MyLibraryService,
              private userBookSvc: UserBookService) {
    translationSvc.initViewTranslation('profile', langMaps);

  }

  openDialog() {
    let dialogRef = this.dialog.open(AddBookComponent, {closeOnNavigation: false});
    dialogRef
      .afterClosed()
      .subscribe(book => {
        this.userBookSvc.addUserBook(book);
      });
  }

  ngOnInit() {
  }

}
