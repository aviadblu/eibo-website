import {AfterContentInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import {GmapService} from "../../../shared/services/gmap.service";
import {UserService} from "../../../shared/services/user.service";
import {IAddress, IUser} from "../../../shared/models/user";
import {GeolocationFieldComponent} from "../geolocation-field/geolocation-field.component";
import {Book} from "../../../shared/models/book";
import {MyLibraryService} from "../../../shared/services/my-library.service";
import {Router} from "@angular/router";

@Component({
  selector: 'eibo-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.less']
})
export class UserProfileComponent implements OnInit{
  public user = null;
  basicForm: FormGroup;
  @ViewChild('geoField') geoField;
  editMode: boolean = false;
  userFollowedBooks: Book[];

  constructor(private userSvc: UserService,
              private libService: MyLibraryService,
              private fb: FormBuilder,
              private gmapSvc: GmapService,
              private router :Router) {
    this.basicForm = fb.group({
      "name": [{value: '', disabled: true}, Validators.required]
    });
  }

  ngOnInit() {
    this.userSvc.userLoggedIn$.subscribe(userInfo => {
      this.user = userInfo;
      this.basicForm.get('name').setValue(this.user.name);
      if (userInfo && userInfo.address) {
        this.geoField.setAddress(userInfo.address);
      }
    });

    this.libService.getUserFollowedBooks().subscribe((books) => {
        books.forEach((b) => {
          b.picture = b.cover_url;
        });
        this.userFollowedBooks = books;
    })
  }

  public onSubmit(): void {
    this.geoField.updateUserAddress();
  }

  navigateToBook(book) {
    this.router.navigate(['/app/book/', book.id])
  }
}
