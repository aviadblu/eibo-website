import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeolocationFieldComponent } from './geolocation-field.component';

describe('GeolocationFieldComponent', () => {
  let component: GeolocationFieldComponent;
  let fixture: ComponentFixture<GeolocationFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeolocationFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeolocationFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
