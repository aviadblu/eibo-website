import {AfterContentInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IAddress, IUser} from "../../../shared/models/user";
import {GmapService} from "../../../shared/services/gmap.service";
import {UserService} from "../../../shared/services/user.service";
import {isNullOrUndefined} from "util";
declare var google;

@Component({
  selector: 'eibo-geolocation-field',
  templateUrl: './geolocation-field.component.html',
  styleUrls: ['./geolocation-field.component.less']
})
export class GeolocationFieldComponent implements OnInit, AfterContentInit {
  public user: IUser = null;
  public address: IAddress;

  private oldAddress = {};
  @ViewChild('search') search;
  @Input() disabled;

  constructor(private userSvc: UserService,
              private fb: FormBuilder,
              private gmapSvc: GmapService) {
  }

  ngOnInit() {
    }

  private guessCurrentLocation() {
    let self = this;
    navigator.geolocation.getCurrentPosition(function (location) {
      const google_map_pos = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
      const google_maps_geocoder = new google.maps.Geocoder();
      google_maps_geocoder.geocode(
        {location: google_map_pos},
        function (results, status) {
          self.address = {
            formatted_address: results[0].formatted_address.split(',').reverse().join(','),
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
          }
        }
      );
    });
  }

  public setAddress(userAddress) {
    this.oldAddress = this.cloneObject(userAddress);
    this.address = this.cloneObject(userAddress);
  }

  private cloneObject(userAddress) {
    return JSON.parse(JSON.stringify(userAddress));
  }

  ngAfterContentInit() {
    this.autoComplete();
  }

  public updateUserAddress(): void {
    if (isNullOrUndefined(this.address) ||
        isNullOrUndefined(this.address.lat) || isNullOrUndefined(this.address.lng) ||
        JSON.stringify(this.oldAddress) == JSON.stringify(this.address)) {
      return;
    }
    this.userSvc.updateUserAddress(this.address);
    this.oldAddress = this.cloneObject(this.address);
  }

  changeAddress(): void {
    if(this.address) {
      this.address.lat = null;
      this.address.lng = null;
    }
  }

  private autoComplete(bounds?) {
    let self = this;
    let autocomplete = this.gmapSvc.createAutoCompleteField(this.search.nativeElement, bounds);
    autocomplete.address$
      .subscribe(address => {
        if (address) {
          self.address = address;
        }
      });
    return autocomplete.autocomplete;
  }
}

