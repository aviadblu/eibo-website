import {Component, Input, OnInit} from '@angular/core';
import {Community, CommunityService} from "../../../../shared/services/community.service";
import {UserService} from "../../../../shared/services/user.service";

@Component({
  selector: 'eibo-communities-list',
  templateUrl: './communities-list.component.html',
  styleUrls: ['./communities-list.component.less']
})
export class CommunitiesListComponent implements OnInit {
  @Input() userId;
  @Input() private fetchAll: boolean = false;
  @Input() showCount = 4;
  @Input() loadMoreCount = 4;
  communities: Community[];

  constructor(public communityService: CommunityService,
              private userService :UserService) { }

  ngOnInit() {
    if(this.userId || this.fetchAll) {
      this.fetchCommunities();
    }

    if(!this.fetchAll) {
      this.userService.userLoggedIn$.subscribe((user) => {
        if (user) {
          if (!this.userId) {
            this.userId = user.id;
            this.fetchCommunities();
          }
        }
      });
    }
  }

  private fetchCommunities() {
    this.getCommunitiesObservable()
      .subscribe((communities) => {
        this.communities = communities;
        this.communityService.userCommunities.subscribe((uc) => {
          this.communityService.updateCommunitiesJoinedStatus(uc, this.communities);
        })
      });
  }

  increaseShowCount() {
    this.showCount += this.loadMoreCount;
  }

  private getCommunitiesObservable() {
    if(this.userId) {
      return this.communityService.profileUserCommunities(this.userId)
    } else {
      return this.communityService.getAll();
    }
  }
}
