import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'eibo-community-card',
  templateUrl: './community-card.component.html',
  styleUrls: ['./community-card.component.less']
})
export class CommunityCardComponent implements OnInit {
  @Input() community;

  constructor() { }

  ngOnInit() {
  }

}
