import { Component, OnInit } from '@angular/core';
import {Community, CommunityService} from "../../../../shared/services/community.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../shared/services/user.service";
import {InfoDialogComponent} from "../../../../shared/components/info-dialog/info-dialog.component";
import {MatDialog} from "@angular/material";
import {DialogAlertComponent} from "../../../../shared/components/dialog-alert/dialog-alert.component";

@Component({
  selector: 'eibo-client-community',
  templateUrl: './client-community.component.html',
  styleUrls: ['./client-community.component.less']
})
export class ClientCommunityComponent implements OnInit {
  community: Community;
  public shareOnWhatsappText;

  constructor(private route: ActivatedRoute,
              private router: Router,
              public communityService: CommunityService,
              public userService: UserService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    let self = this;
    this.route.params.subscribe(value => {
      self.communityService.getCommunity(value.communityId).subscribe((c) => {
        if (c instanceof Array) {
          c = c[0];
        }
        self.community = c;
        self.setShareOnWhatsappText();

        self.communityService.userCommunities.subscribe((uc) => {
          self.communityService.updateCommunitiesJoinedStatus(uc, [self.community]);
        })
      }, () => this.router.navigate(['/']))
    });
  }

  setShareOnWhatsappText() {
    this.shareOnWhatsappText =
      window.location.href + `
קבלת הזמנה מ EiBO להצטרף לקבוצת הקריאה של ${this.community.name} 
הזמן עוד חברים על מנת ליצור קהילת קוראים גדולה יותר`;
  }

  userIsGroupOwner() {
    return this.userService.userLoggedIn$.getValue() && this.community &&
      this.userService.userLoggedIn$.getValue().id == this.community.owner_id
  }

  parse(str) {
    let args = [].slice.call(arguments, 1),
      i = 0;

    return str.replace(/%s/g, function() {
      return args[i++];
    });
  }
}
