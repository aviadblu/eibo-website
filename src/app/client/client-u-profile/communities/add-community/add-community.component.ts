import {GmapService} from '../../../../shared/services/gmap.service';
import {CommunityService, SearchResultCommunity, Community} from '../../../../shared/services/community.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

import {AfterContentInit, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {IAddress} from "../../../../shared/models/user";
import {catchError, debounceTime, distinctUntilChanged, switchMap} from "rxjs/internal/operators";

@Component({
  selector: 'eibo-add-community',
  templateUrl: './add-community.component.html',
  styleUrls: ['./add-community.component.less']
})
export class AddCommunityComponent implements OnInit, AfterContentInit {
  @ViewChild('inputAddress') inputAddress;

  private SearchTerm = new Subject<string>();
  communityForm: FormGroup;
  communities$: Observable<any[]>;
  selected: Community;
  showForm: boolean;
  isEdit: boolean;
  public address: IAddress = {
    formatted_address: "",
    lat: 0,
    lng: 0
  };

  constructor(public comService: CommunityService,
              private fb: FormBuilder,
              private gmapSvc: GmapService,
              private dialogRef: MatDialogRef<AddCommunityComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  private autoComplete() {
    this.gmapSvc.createAutoCompleteField(this.inputAddress.nativeElement)
      .address$
      .subscribe(address => {
        this.communityForm.get('address').setValue(<IAddress>address);
      });
  }

  updateCommunityPicture(url) {
    this.communityForm.controls["logo"].setValue(url);
  }

  public search(term: string) {
    this.SearchTerm.next(term);
  }

  public checkAddress(address: IAddress) {
    return (this.communityForm.get('address').value.lat && this.communityForm.get('address').value.lng) || false;
  }

  public createCommunity(communityName: string) {
    this.setCommunityForm(null, this.address, communityName, "", "");
    this.showForm = true;
  }

  public joinCommunity(community: Community) {
    this.comService.joinCommunity(community).subscribe(() => {
      this.closeDialog();
    });
  }

  public communitySelected(community: Community) {
    this.selected = community;
  }

  private setCommunityForm(id: number, address: any, name: string, logo: string, owner_id: string) {
    this.communityForm.setValue({
      id: id,
      name: name,
      address: address,
      logo: logo,
      owner_id: owner_id
    });

  }

  public closeDialog() {
    this.dialogRef.close();
  }

  ngAfterContentInit() {
    this.autoComplete();
  }

  public changeAddress(): void {
    this.communityForm.get('address').patchValue({
      formatted_address: this.communityForm.get('address').value.formatted_address,
      lat: null,
      lng: null
    })
  }

  public onSubmitCommunity(Community: Community, isValid: boolean) {
    if (isValid && this.checkAddress(Community.address)) {
      if (this.data != null && this.data['communityId']) {
        this.comService.editCommunity(Community, Community.id).subscribe(() => {
          this.closeDialog();
        });
      } else {
        this.comService.addCommunity(Community).subscribe(res => {
          this.comService.joinCommunity(res.id).subscribe(() => {
            this.closeDialog();
          });
        });
      }
    }
  }

  ngOnInit() {
    this.communityForm = this.fb.group({
      id: [],
      address: [this.address],
      name: ["", Validators.required],
      logo: [""],
      owner_id: [""]
    });

    if (this.data != null) {
      this.showForm = true;
      this.isEdit = true;
      this.comService.getCommunity(this.data['communityId']).subscribe((res :Community) => {
        this.setCommunityForm(res[0].id, res[0].address, res[0].name, res[0].logo, res[0].owner_id);
      });
    }

    this.communities$ = this.SearchTerm
      .pipe(debounceTime(300), distinctUntilChanged(),
      switchMap(term =>
        term ? this.comService.getCommunityAutoComplete(term) : of<SearchResultCommunity[]>([])
      ),
      catchError(error => {
        console.log(error);
        return of<SearchResultCommunity[]>([])
      }));
  }

}
