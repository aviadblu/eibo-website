import { CommunityService } from '../../../../shared/services/community.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogDeleteComponent } from '../../../../shared/components/dialog-delete/dialog-delete.component';

@Component({
  selector: 'eibo-my-communities',
  templateUrl: './my-communities.component.html',
  styleUrls: ['./my-communities.component.less']
})
export class MyCommunitiesComponent implements OnInit {
  @Input() communities:any[];
  @Output() onEdit:EventEmitter<any> = new EventEmitter();
  constructor(public dialog: MatDialog,public comService:CommunityService) { }
  leaveCommunity(communityUserId:number) {
    let dialog = this.dialog.open(DialogDeleteComponent,{
      data: {caption:'Do you wish to leave this community?'}
    });
    dialog.afterClosed().subscribe((confirm)=>{
      if(confirm) {
        this.comService.leaveCommunity(communityUserId).subscribe(res => {
          this.communities = this.comService.userCommunities.getValue();
        });
      }
    });
  }
  onEditCommunity(communityId: number):void {
    this.onEdit.emit(communityId);
  }
  ngOnInit() {
  }

}
