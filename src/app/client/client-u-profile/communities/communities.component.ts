import { CommunityService } from '../../../shared/services/community.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { AddCommunityComponent } from './add-community/add-community.component';

@Component({
  selector: 'eibo-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.less']
})
export class CommunitiesComponent implements OnInit {
  userCommunities$: any;
  constructor(public dialog: MatDialog , public comService:CommunityService) {
    this.userCommunities$ = this.comService.userCommunities;
  }
  openDialog() {
    let dialogRef = this.dialog.open(AddCommunityComponent, {closeOnNavigation: false});
  }
  editCommunity(communityId: number) {
    let dialogRed = this.dialog.open(AddCommunityComponent, {
      data: {communityId: communityId},
      closeOnNavigation: false
    });
  }
  ngOnInit() {
  }

}
