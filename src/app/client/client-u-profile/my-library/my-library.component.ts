import {Component, OnInit} from '@angular/core';
import { MatDialog} from '@angular/material';
import { Observable ,  Subject } from 'rxjs';

import { MyLibraryService } from '../../../shared/services/my-library.service';
import { UserBookService } from '../../../shared/services/user-book.service';
import { AddBookComponent } from './add-book/add-book.component';
import { ReviewBookComponent } from 'app/client/client-u-profile/my-library/review-book/review-book.component';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from '../../../shared/services/user.service';
import {Book} from "../../../shared/models/book";

@Component({
  selector: "eibo-my-library",
  templateUrl: "./my-library.component.html",
  styleUrls: ["./my-library.component.less"]
})
export class MyLibraryComponent implements OnInit {

  userBooks$: any;
  userChildrenBooks$;
  userLearningBooks$;
  userReadingBooks$;
  userNonfictionBooks$;

  bookForm: FormGroup;
  isCollapsedBookForm = true;

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private userBookSvc: UserBookService,
    private userSvc: UserService,
    private route: ActivatedRoute
  ) {
    this.bookForm = this.fb.group({
      cover_url: [""],
      name: ["", Validators.required],
      author: ["", Validators.required]
    });
    this.userBookSvc.loadUserBooks();

    this.userChildrenBooks$ = this.userBookSvc.userChildrenBooks;
    this.userReadingBooks$ = this.userBookSvc.userReadingBooks;
    this.userNonfictionBooks$ = this.userBookSvc.userNonfictionBooks;
    this.userLearningBooks$ = this.userBookSvc.userLearningBooks;
    this.userBooks$ = this.userChildrenBooks$;
  }

  public createBook(bookName: string) {
    let dialogRef = this.dialog.open(AddBookComponent, {
      data: { bookName },
      closeOnNavigation: false
    });
  }

  public collapseBookForm() {
    this.isCollapsedBookForm = !this.isCollapsedBookForm;
    this.userSvc
      .updateApplicationOptions("isCollapsedBookForm", this.isCollapsedBookForm)
      .then((res) => console.log(res))
      .catch(err => {
        console.log(err);
      });
  }

  public editUserBook(b: Book) {
    let dialogRef = this.dialog.open(ReviewBookComponent,{
      data: { userBookId: b.bookuserid },
      panelClass:'no-padding',
      width: '80%',
      height: '90%',
      closeOnNavigation: false
    });
  }

  public createReview(book) {
    this.bookForm.get('name').setValue('');
    let dialogRef = this.dialog.open(ReviewBookComponent, {
      data: { book },
      panelClass:'no-padding',
      width: '80%',
      height: '90%',
      closeOnNavigation: false
    });
  }

  public updateBookCover(url) {
    this.bookForm.controls["cover_url"].setValue(url);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params["openAddBook"]) {
        this.isCollapsedBookForm = false;
      }
    });
    this.userSvc.app_options$.subscribe((app_options)=>{
      this.isCollapsedBookForm = app_options.isCollapsedBookForm;
    })
  }
}
