import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SearchResultBook} from "../../../../shared/models/search-result-book";
import {Observable, BehaviorSubject, of} from "rxjs";
import {MyLibraryService} from "../../../../shared/services/my-library.service";
import {FormControl} from "@angular/forms";
import {catchError, debounceTime, distinctUntilChanged, switchMap} from "rxjs/internal/operators";

@Component({
  selector: 'eibo-book-selector',
  templateUrl: './book-selector.component.html',
  styleUrls: ['./book-selector.component.less']
})
export class BookSelectorComponent implements OnInit {

  public searchTerm$ = new BehaviorSubject<string>('');
  @Output() bookSelected = new EventEmitter<SearchResultBook>();
  searchControl = new FormControl('');
  @Input() inputPlaceholder: string = 'Find book';
  noResults: boolean = false;
  @Output() noResultOptionClicked = new EventEmitter<string>();
  public bookSearchResults: any;

  constructor(private libSvc: MyLibraryService) { }

  ngOnInit() {
    this.changeBookSearchResultsOnSearchChanged();
  }

  public search(term: string) {
    this.searchTerm$.next(term);
  }

  private changeBookSearchResultsOnSearchChanged() {
    this.searchTerm$.pipe(debounceTime(300),
      distinctUntilChanged())
      .subscribe(term => {
        if (term && term.length > 1)
          this.libSvc.getBooksAutocomplete(term).subscribe(res => {
            this.noResults = ((<any>res).length === 0);
            this.bookSearchResults = res;
          });
      });
  }

}
