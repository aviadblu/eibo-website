import { ReviewBookComponent } from '../review-book/review-book.component';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog,MatDialogRef,MAT_DIALOG_DATA } from '@angular/material';

import { Book } from './../../../../shared/models/book';
import { MyLibraryService } from './../../../../shared/services/my-library.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "eibo-add-book",
  templateUrl: "add-book.component.html",
  styleUrls: ["./add-book.component.less"]
})
export class AddBookComponent implements OnInit {

  bookForm: FormGroup;

  isSubmit:boolean;
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private myLibraryService: MyLibraryService,
    private dialogRef: MatDialogRef<AddBookComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
    this.bookForm = this.fb.group({
      cover_url: [""],
      name: ["", Validators.required],
      author: ["", Validators.required]
    });
    if (this.data != null) {
      this.createBook(data['bookName'])
    }
  }

  updateBookCover(url) {
    this.bookForm.controls["cover_url"].setValue(url);
  }

  public createReview(book) {
    let dialogRed = this.dialog.open(ReviewBookComponent, {
      data: {book},
      panelClass:'no-padding',
      width: '80%',
      height: '90%',
      closeOnNavigation: false
    });
  }

  public createBook(bookName: string) {
    this.bookForm.get("name").setValue(bookName);
  }

  public onSubmitBook(book: Book, isValid: boolean) {
    this.isSubmit = true;
    if (isValid) {
      this.myLibraryService.addBook(book).subscribe(res => {
        this.dialogRef.close(res);
        this.isSubmit = false;
      });
    }
  }
  ngOnInit() {
    this.dialogRef.afterClosed().subscribe(book => {
      if(book) this.createReview(book);
    });
  }
}
