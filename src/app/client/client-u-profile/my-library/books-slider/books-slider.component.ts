import { MatDialog } from '@angular/material';
import {Component, Input, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {Book} from "../../../../shared/models/book";
import {BookReviewDialogComponent} from "../../../book-review-dialog/book-review-dialog.component";
import {Constants} from "../../../../constants";
import {Router} from "@angular/router";

@Component({
  selector: 'eibo-books-slider',
  templateUrl: './books-slider.component.html',
  styleUrls: ['./books-slider.component.less']
})
export class BooksSliderComponent implements OnInit {
  @Output() lastSlideReached: EventEmitter<number> = new EventEmitter<number>();
  public EIBO_LOGO;

  constructor(public dialog:MatDialog) {
    this.EIBO_LOGO = Constants.EIBO_LOGO;
  }

  @Input() books: Book[];
  @Output() onSlideClick: EventEmitter<any> = new EventEmitter();
  @Input() hideButtons: boolean;
  @Input() withWriteReview: boolean = false;
  config: SwiperOptions = {
    spaceBetween: 19,
    slidesPerView: 3.3,
    freeMode: true,
    slidesOffsetBefore: 15,
    grabCursor: true,
    onSliderMove: this.emitWhenLastSliderDisplayed.bind(this)
  };

  ngOnInit() {
  }

  removePicture(book: Book) {
    book.picture = '';
  }

  openReviewModal(book: Book) {
    let dialog = this.dialog.open(BookReviewDialogComponent, {
      data: {userBook: book},
      width: '90%',
      disableClose: true,
      closeOnNavigation: false
    });
    dialog.afterClosed().subscribe((returnedBook) => {
      if(returnedBook) {
        this.books[this.books.indexOf(book)] = returnedBook;
        Object.assign(book, returnedBook);
      }
    })
  }

  trackByFunc(book) {
    return book.id;
  }

  private emitWhenLastSliderDisplayed(s :Swiper) {
    if(s.activeIndex >= s.slides.length - 5) { //the leftmost slide is defined active
      this.lastSlideReached.emit(s.slides.length);
    }
  }
}
