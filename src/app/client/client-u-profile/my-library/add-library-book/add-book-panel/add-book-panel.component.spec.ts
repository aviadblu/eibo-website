import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBookPanelComponent } from './add-book-panel.component';

describe('AddBookPanelComponent', () => {
  let component: AddBookPanelComponent;
  let fixture: ComponentFixture<AddBookPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBookPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBookPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
