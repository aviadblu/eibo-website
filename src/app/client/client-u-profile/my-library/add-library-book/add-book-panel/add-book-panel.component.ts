import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {Book} from "../../../../../shared/models/book";
import {MyLibraryService} from "../../../../../shared/services/my-library.service";
import {Observable} from "rxjs";

@Component({
  selector: 'eibo-add-book-panel',
  templateUrl: './add-book-panel.component.html',
  styleUrls: ['./add-book-panel.component.less']
})
export class AddBookPanelComponent implements OnInit {

  bookForm: FormGroup;
  isSubmit: boolean;
  @Input() bookName: string | Observable<string>;

  constructor(
    private fb: FormBuilder,
    private libSvc: MyLibraryService) { }

  ngOnInit() {
    this.bookForm = this.fb.group({
      cover_url: [""],
      name: [this.bookName || '', this.requireNameValidator],
      author: ["", Validators.required]
    });
  }

  public submitBook(callback) {
    if (this.bookForm.valid) {
      this.isSubmit = true;
      this.libSvc.addBook(this.bookForm.value).subscribe(res => {
        this.isSubmit = false;
        this.clearBookForm();
        callback(res.id);
      });
    }
  }

  public updateBookCover(url) {
    this.bookForm.controls["cover_url"].setValue(url);
  }

  private clearBookForm() {
    this.bookForm.get('name').setValue('');
    this.bookForm.get('author').setValue('');
    this.updateBookCover(null);

    this.bookForm.get('name').setErrors(null);
    this.bookForm.get('author').setErrors(null);
  }


  isFormDisabled() {
    return this.bookForm.get('author').value == '' ||
      this.requireNameValidator(this.bookForm.get('name')) != null;
  }

  requireNameValidator(control: AbstractControl): {[key: string]: any} | null {
    let nameValue = control.value;
    return (typeof nameValue !== 'string' && nameValue.getValue() === '' ||
      nameValue === '') ? {'requireName': 'name is empty'} : null;
  }
}
