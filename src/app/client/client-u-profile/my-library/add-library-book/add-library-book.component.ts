import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ReviewBookComponent} from "../review-book/review-book.component";
import {MatDialog} from "@angular/material";
import {BookSelectorComponent} from "../book-selector/book-selector.component";
import {MyLibraryService} from "../../../../shared/services/my-library.service";
import {ErrorDialogComponent} from "../../../../shared/components/error-dialog/error-dialog.component";
import {Book} from "../../../../shared/models/book";

@Component({
  selector: 'eibo-add-library-book',
  templateUrl: './add-library-book.component.html',
  styleUrls: ['./add-library-book.component.less']
})
export class AddLibraryBookComponent implements OnInit {

  @ViewChild(BookSelectorComponent) bookSelector;
  @Output() addPressed = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private libSvc: MyLibraryService) { }

  ngOnInit() {
  }

  public createReviewAndAddNewBook(bookName : string) {
    this.createReview({bookName})
  }

  public createReviewForExistingBook(book : {book: Book}) {
    this.createReview({book: book})
  }

  public createReview(book : {bookName} | {book}) {
    let dialogRef = this.dialog.open(ReviewBookComponent, {
      data: book,
      panelClass:'no-padding',
      width: '80%',
      height: '90%',
      closeOnNavigation: false
    });
  }

  public fetchBook(barcode) {
    this.libSvc.getBookByBarcode(barcode).subscribe(res => {
      this.createReview({book: res});
    }, (err) => {
      let dialogRef = this.dialog.open(ErrorDialogComponent, {
        width: '250px',
        data: {error: err, title: 'Alert'}
      });
    });
  }
}
