import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLibraryBookComponent } from './add-library-book.component';

describe('AddLibraryBookComponent', () => {
  let component: AddLibraryBookComponent;
  let fixture: ComponentFixture<AddLibraryBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLibraryBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLibraryBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
