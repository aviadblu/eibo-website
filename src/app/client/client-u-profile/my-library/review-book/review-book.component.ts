import { UserBookService } from '../../../../shared/services/user-book.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Component, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material'
import {AddBookPanelComponent} from "../add-library-book/add-book-panel/add-book-panel.component";
import {DialogDeleteComponent} from "../../../../shared/components/dialog-delete/dialog-delete.component";
import {Constants} from "../../../../constants";
@Component({
  selector: 'eibo-review-book',
  templateUrl: './review-book.component.html',
  styleUrls: ['./review-book.component.less']
})
export class ReviewBookComponent implements OnInit {
  READING_CATEGORY = UserBookService.READING_CATEGORY;
  CHILDREN_CATEGORY = UserBookService.CHILDREN_CATEGORY;
  NONFICTION_CATEGORY = UserBookService.NONFICTION_CATEGORY;
  LEARNING_CATEGORY = UserBookService.LEARNING_CATEGORY;
  CATEGORIES_NAMES = UserBookService.TRANSLATE_CATEGORIES['en'];

  bookUserInfo = {
    name:"",
    cover_url:"",
    bookUserId: null
  };

  reviewForm: FormGroup;
  isSubmit:boolean;
  uploadNewBook:boolean = false;
  @ViewChild(AddBookPanelComponent) addBookPanel;
  EIBO_LOGO = Constants.EIBO_LOGO;

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ReviewBookComponent>,
    private userBookSvc: UserBookService,
    public dialog:MatDialog
  ) {
    this.reviewForm = this.fb.group({
      bookId: [],
      rating: 0,
      review: [""],
      condition: [""],
      ready_for_swap: [true],
      category: ['reading']
    });
    //if userbook data passed to dialog
    if (this.data != null) {
      if(this.data['userBookId']) {
        userBookSvc.getUserBook(this.data['userBookId']).subscribe(res => {
          this.setbookUserInfo(res.name, res.cover_url, res.bookuserid);
          this.setReviewForm(parseInt(res.bookid),res.rating,res.review,res.condition,
            res.ready_for_swap, res.category);
        });
      } else if(this.data['book']) {
        this.setbookUserInfo(this.data['book']['name'],this.data['book']['cover_url'],null);
        this.setReviewForm(parseInt(this.data.book.id),0,"","new",true, UserBookService.READING_CATEGORY);
      } else {
        this.uploadNewBook = true;
        this.setReviewForm(null,0,"","new",true, UserBookService.READING_CATEGORY);
      }
    }
  }

  ngOnInit() {
  }
  public closeDialog() {
    this.userBookSvc.loadUserBooks();
    this.dialogRef.close();
  }

  setbookUserInfo(name:string,cover_url:string,bookUserId:number) {
    this.bookUserInfo.name = name;
    this.bookUserInfo.cover_url = cover_url;
    this.bookUserInfo.bookUserId = bookUserId;
  }

  private setReviewForm(bookId:number,rating:number,review:string,
                        condition:string,ready_for_swap:boolean,category:string) {
    this.reviewForm.setValue({
      bookId: bookId,
      rating: rating,
      review: review,
      condition: condition,
      ready_for_swap: ready_for_swap,
      category: category
    });
  }

  public onSubmitReview() {
    if(this.addBookPanel) {
      this.addBookPanel.submitBook((bookId) => {
        let userBook = this.reviewForm.value;
        userBook.bookId = bookId;
        this.submitReview(this.reviewForm.value, this.reviewForm.valid)
      })
    } else {
      this.submitReview(this.reviewForm.value, this.reviewForm.valid)
    }
  }

  public submitReview(userBook, isValid: boolean) {
    this.isSubmit = true;
    if (isValid) {
      if (this.bookUserInfo != null && this.bookUserInfo['bookUserId'] != null) {
        this.userBookSvc.editUserBook(this.bookUserInfo.bookUserId, userBook).subscribe(() => {
          this.isSubmit = false;
          this.closeDialog();
        });
      } else {
        this.userBookSvc.addUserBook(userBook).subscribe(() => {
          this.isSubmit = false;
          this.closeDialog();
        });
      }
    }
  }

  isAddBookPanelValid() {
    if(!this.addBookPanel || this.addBookPanel.bookForm.valid) {
      return true;
    }
    return false;
  }

  deleteBook(bookUserId: number) {
    let dialog = this.dialog.open(DialogDeleteComponent,{
      data: {caption:'Do you wish to delete this book?'}
    });

    dialog.afterClosed().subscribe((confirm)=>{
      if(confirm) {
        this.isSubmit = true;
        this.userBookSvc.deleteUserBook(bookUserId).subscribe(res => {
          this.isSubmit = false;
          this.userBookSvc.loadUserBooks();
          this.closeDialog();
        });
      }
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  preventUnsavedChanges() {
    return false;
  }
}
