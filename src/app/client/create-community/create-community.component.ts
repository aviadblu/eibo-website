import {Component, Input, OnInit} from '@angular/core';
import {AddCommunityComponent} from "../client-u-profile/communities/add-community/add-community.component";
import {MatDialog} from "@angular/material";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Community, CommunityService, UserCommunityResponse} from "../../shared/services/community.service";
import {UserService} from "../../shared/services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {InfoDialogComponent} from "../../shared/components/info-dialog/info-dialog.component";
import {ErrorDialogComponent} from "../../shared/components/error-dialog/error-dialog.component";
import {DialogAlertComponent} from "../../shared/components/dialog-alert/dialog-alert.component";

@Component({
  selector: 'eibo-create-community',
  templateUrl: './create-community.component.html',
  styleUrls: ['./create-community.component.less']
})
export class CreateCommunityComponent implements OnInit {
  @Input() community :Community;
  newGroupForm: FormGroup;

  constructor(fb: FormBuilder,
              private communityService: CommunityService,
              private userSvc: UserService,
              private router: Router,
              public dialog: MatDialog,
              public route:ActivatedRoute) {
    this.newGroupForm = fb.group({
      groupName: ['', Validators.required],
      groupDescription: ['',
        Validators.compose([Validators.maxLength(500)])],
      logo : [null, Validators.required]
    })
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.communityId) {
        window.scroll(0,0);
        this.communityService.getCommunity(params.communityId)
          .subscribe((c: Community) => {
            this.community = c;
            this.newGroupForm.get('logo').setValue(c.logo);
            this.newGroupForm.get('groupName').setValue(c.name);
            this.newGroupForm.get('groupDescription').setValue(c.description);
          })
      }
    });
  }

  createOrUpdateNewGroup() {
    if(this.community) {
      this.updateCommunity();
    } else {
      this.createNewGroup();
    }
  }

  private updateCommunity() {
    this.community.logo = this.newGroupForm.value.logo;
    this.community.name = this.newGroupForm.value.groupName;
    this.community.description = this.newGroupForm.value.groupDescription;

    this.communityService.updateCommunity(this.community)
      .subscribe(() => {
        this.dialog.open(InfoDialogComponent, {data: {
            caption: 'Group updated successfully',
            onConfirm:  () => this.router.navigate(['/app/u/community', this.community.id])
          }});
      })
  }

  createNewGroup() {
    this.communityService.addCommunity(new Community(
      null, this.newGroupForm.value.groupName, this.newGroupForm.value.groupDescription, null,
      this.newGroupForm.value.logo,
      this.userSvc.userLoggedIn$.getValue().id
    )).subscribe((c) => {
      this.communityService.joinCommunity(c).subscribe(() => {
        this.dialog.open(InfoDialogComponent, {data: {
            caption: 'Group created!',
            description: 'Your Group has been sent for approval',
            onConfirm:  () => this.router.navigate(['/'])
          }});
      });
    });
  }

  updateGroupImage(image: boolean) {
    this.newGroupForm.controls.logo.setValue(image);
  }

  deleteCommunity() {
    let self = this;
    this.dialog.open(DialogAlertComponent, {
      data: {
        caption: 'Are you sure you what to delete your group?',
        onConfirmed: () => {
          self.communityService.deleteCommunity(self.community).subscribe(() => {
            self.dialog.open(InfoDialogComponent, {
              data: {
                caption: 'Group deleted',
                description: 'Your Group has been deleted',
                onConfirm: () => self.router.navigate(['/'])
              }
            });
          })
        }
      }
    })
  }
}
