import { Router } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DialogDeleteComponent } from '../../shared/components/dialog-delete/dialog-delete.component';
import { ChatsService } from '../../shared/services/chats.service';
import { GeneralService } from '../../shared/services/general.service';
import { UserBookService } from '../../shared/services/user-book.service';
import { UserService } from '../../shared/services/user.service';
import {GmapService} from "../../shared/services/gmap.service";
import {Suggestion} from "../types/suggestion.module";

@Component({
  selector: "eibo-client-slider",
  templateUrl: "./client-slider.component.html",
  styleUrls: ["./client-slider.component.less"]
})
export class ClientSliderComponent implements OnInit, OnDestroy {
  selectedIndex = 1;
  selectedInfo: any;
  SliderOffset = 0;
  isMobile = false;

  SWIPE_ACTION = { LEFT: "swipeleft", RIGHT: "swiperight" };

  items: any;

  private subs = [];

  constructor(
    private dialog: MatDialog,
    private bo: BreakpointObserver,
    private genSvc: GeneralService,
    private gmapsSvc: GmapService,
    private chatSvc: ChatsService,
    private userBookSvc: UserBookService,
    private userSvc: UserService,
    private router:Router

  ) {
    const usersSub = this.genSvc.userSuggestions$.subscribe((suggestions : Suggestion[]) => {

      if (suggestions) {
        this.items = suggestions;
        this.selectedIndex = this.items.length > 1 ? 1 : 0;
        this.selectedInfo = this.items[this.selectedIndex];

        this.gmapsSvc.getCurrentUserLocation((userLocation) => {
          suggestions.forEach((suggestion) => {
            this.gmapsSvc.computeUsersDistances(userLocation, suggestion.users);
          });
        });
      }
    });
    this.subs.push(usersSub);
  }

  private rotateSelected(n: number, max: number, add: boolean): number {
    return add ? (n < max ? n + 1 : 0) : n > 0 ? n - 1 : max;
  }

  private limitSelected(n: number, max: number, add: boolean): number {
    return add ? (n < max ? n + 1 : max) : n > 0 ? n - 1 : 0;
  }

  slideLeft() {
    this.SliderOffset =
      this.selectedIndex == 0
        ? this.SliderOffset
        : this.SliderOffset + (this.isMobile ? 100 : 33.333333);
    this.selectedIndex = this.limitSelected(
      this.selectedIndex,
      this.items.length - 1,
      false
    );
    this.selectedInfo = this.items[this.selectedIndex];
  }

  slideRight() {
    this.SliderOffset =
      this.selectedIndex == this.items.length - 1
        ? this.SliderOffset
        : this.SliderOffset - (this.isMobile ? 100 : 33.333333);
    this.selectedIndex = this.limitSelected(
      this.selectedIndex,
      this.items.length - 1,
      true
    );
    this.selectedInfo = this.items[this.selectedIndex];
  }

  swipe(currentIndex: number, action = this.SWIPE_ACTION.RIGHT) {
    if (action === this.SWIPE_ACTION.RIGHT) {
      this.slideLeft();
    }
    if (action === this.SWIPE_ACTION.LEFT) {
      this.slideRight();
    }
  }

  swap(book, user) {
/*     if(this.userSvc.user['books_num']>0) { */
          let dialog = this.dialog.open(DialogDeleteComponent, {
            data: {
              caption: "Do you want to swap " + book.bookName + "?"
            }
          });
          dialog.afterClosed().subscribe(confirm => {
            if (confirm) {
              this.chatSvc
                .checkCreateNewChat(user.uid)
                .then((chatId: string) => {
                  this.chatSvc
                    .bookSwap(
                      chatId,
                      {
                        id: user.uid,
                        pg: user.id,
                        name: user.name,
                        photo: user.photo,
                        distance: 2.5
                      },
                      book
                    )
                    .then(chatId => {
                      this.router.navigate(["/app/u/chat/", chatId]);
                    });
                });
            }
          });
/*         } else {
          let dialog = this.dialog.open(DialogAlertComponent, {
            data: {
              caption:
                "In order to swap books you need to add books to your library.",
              route: "/app/u/profile/library"
            }
          });
          dialog.afterClosed().subscribe(route => {
            if (route) {
              this.router.navigate([route]);
            }
          });
        } */
  }

  ngOnDestroy() {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }
  ngOnInit() {
    this.userSvc.userLoggedIn$.subscribe((user) => {
      if(user) {
        this.genSvc.loadUserSuggestions(user.id);
        this.bo.observe([
          "(max-width: 599px) and (orientation: portrait), (max-width: 766px) and (orientation: landscape)"
        ])
          .subscribe(result => {
            this.isMobile = result.matches;
            this.SliderOffset = this.isMobile ? -100 : 0;
          });
      }
    });
  }
}
