import {forkJoin, Observable} from "rxjs";
import { UserBookService } from "../../shared/services/user-book.service";
import { CommunityService } from "../../shared/services/community.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { UserService } from "../../shared/services/user.service";
import { CloudinaryHelperService } from "../../shared/services/cloudinary-helper.service";
import {map, switchMap} from "rxjs/internal/operators";
import {Book} from "../../shared/models/book";
import {User} from "../types/user.module";
import {MyLibraryService} from "../../shared/services/my-library.service";

@Component({
  selector: "eibo-client-profile",
  templateUrl: "./client-profile.component.html",
  styleUrls: ["./client-profile.component.less"]
})
export class ClientProfileComponent implements OnInit {
  profileData: any;
  userId: string;

  constructor(
    private route: ActivatedRoute,
    public userService: UserService,
    public communityService: CommunityService,
    public userBookService: UserBookService
  ) {}

  getProfile(uid: string): Observable<any> {
    return forkJoin([
      this.userService.loadUserData(uid),
      this.userBookService.profileUserBooks(uid)
    ]).pipe(map((data: any[]) => {
      let userData: any = data[0];
      userData.thumbnail = CloudinaryHelperService.getUrlWithImageResize(userData.photo, 100, 100);
      userData.books = data[1];
      return userData;
    }));
  }

  ngOnInit() {
    this.route.paramMap
      .pipe(switchMap((params: ParamMap) => {
        this.userId = params.get("uid");
        return this.getProfile(this.userId);
      }))
      .subscribe(res => {
        this.profileData = res;
      });
  }

  removePicture(book: Book) {
    book.picture = '';
  }
}
