import {Location} from "./location.module";

export class User {
  id : number;
  userid : number;
  username : string;
  userphoto : string;
  communityname : string;
  address?: Location;
  distance?: number;
  app_options;


  constructor(userid?: number, username?: string, userphoto?: string, communityname?: string, address?: Location) {
    this.userid = userid;
    this.username = username;
    this.userphoto = userphoto;
    this.communityname = communityname;
    this.address = address;
  }
}
