import {User} from "./user.module";

export interface Suggestion {
  bookAuthor: string
  bookCover: string
  bookId: string
  bookName: string
  bookPage: string
  reviewCount: number
  reviewScore: number
  users: User[];
}
