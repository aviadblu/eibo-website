import {Component, OnInit} from '@angular/core';
import {UserService} from "../../shared/services/user.service";

@Component({
  selector: 'eibo-client-admin',
  template: `
    <eibo-loader *ngIf="!user"></eibo-loader>
    <router-outlet *ngIf="auth && user && user.admin"></router-outlet>
    <div *ngIf="!auth">Please login</div>
  `
})
export class ClientAdminComponent implements OnInit {
  auth = false;
  user = null;

  constructor(private userSvc: UserService) {

    this.userSvc.auth$.subscribe(auth => {
      this.auth = auth;
    });

    this.userSvc.user$.subscribe(userInfo => {
      this.user = userInfo;
    });
  }

  ngOnInit() {
  }

}
