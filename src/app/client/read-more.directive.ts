import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[eiboReadMore]'
})
export class ReadMoreDirective implements OnInit {

  @HostBinding('class.read-more')
  readMore : boolean = true;

  @HostBinding('class.read-less')
  readLess : boolean = false;

  @HostListener('touchstart') onMouseLeave() {
    this.toogleReadMore(!this.readMore);
  }

  constructor(el: ElementRef) {
    if(el.nativeElement.innerHTML.length > 24) {
      this.toogleReadMore(false);
    }
  }

  ngOnInit() {

  }

  toogleReadMore(readMore :boolean) {
    if(readMore) {
      this.readMore = true;
      this.readLess = false;
    } else {
      this.readMore = false;
      this.readLess = true;
    }
  }
}
