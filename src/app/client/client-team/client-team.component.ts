import {Component, OnInit} from '@angular/core';
import {langMaps} from './team.text';
import {TranslationService} from "../../shared/services/translation.service";

@Component({
  selector: 'eibo-client-team',
  templateUrl: './client-team.component.html',
  styleUrls: ['./client-team.component.less']
})
export class ClientTeamComponent implements OnInit {

  constructor(private translationSvc: TranslationService) {
    translationSvc.initViewTranslation('team', langMaps);
  }

  ngOnInit() {
  }

  translate(key) {
    return this.translationSvc.translate(key);
  }

}
