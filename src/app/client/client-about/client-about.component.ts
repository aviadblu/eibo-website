import { Component, OnInit,ElementRef } from '@angular/core';

import { TranslationService } from '../../shared/services/translation.service';
import { langMaps } from './about.text';

@Component({
  selector: 'eibo-client-about',
  templateUrl: './client-about.component.html',
  styleUrls: ['./client-about.component.less']
})
export class ClientAboutComponent implements OnInit {


  video_id = "promo_video/VID-20171117-WA0001";
  public isVideoPlaying = false;
  constructor(private translationSvc: TranslationService,private elRef: ElementRef) {
    translationSvc.initViewTranslation('about', langMaps);
  }

  ngOnInit() {
  }

  videoPlay() {
    this.elRef.nativeElement.querySelector(`video`).play();
    this.isVideoPlaying = true;
  }

  videoPause() {
    this.elRef.nativeElement.querySelector(`video`).pause();
    this.isVideoPlaying = false;
  }
  translate(key) {
    return this.translationSvc.translate(key);
  }

}
