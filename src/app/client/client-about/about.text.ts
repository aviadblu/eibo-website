const en = {
  lang: 'en',
  "pageHeadline": `About`,
  "intro": `
  With our apps we want to turn free community book swapping and giving into an easy, accessible, and fun experience. We are incentivising development of communities both as an independent goal, and a ground for active and growing local market. EiBO works to deliver social, environment, and business impacts to local citizens, communities and organizations on all levels turning private bookshelves into free community smart libraries.
  `,
  "missionHeadline": `Mission`,
  "missionContent": `Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the
              digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information
              highway will close the loop on focusing solely on the bottom line.`,
  "team": 'Team'
};

const he = {
  lang: 'he',
  "pageHeadline": `אודות`,
  "intro": `החיבור לסביבה הקרובה שלנו, לאנשים שמסביבנו, ולקהילות אליהן אנחנו שייכים ובהן אנו פועלים, מאפשרים לנו לעשות שינויים בתרבות הצריכה שלנו. שינויים שהופכים את החיים שלנו לפשוטים יותר, נוחים יותר, ירוקים יותר וחסכוניים יותר. איבו היא פלטפורמה חדשנית המאפשרת לנו למצוא כל ספר בדרך פשוטה נוחה וחסכונית יותר דרך משתמשים אחרים הנמצאים בסביבת העבודה, המגורים, או קהילת החברים אליה אנו שייכים בכל מקום בעולם.`,
  "missionHeadline": `Mission`,
  "missionContent": ``,
  "team": "צוות"
};

export const langMaps = [en,he];
