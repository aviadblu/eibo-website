import {Component, OnInit} from '@angular/core';
import {LangService} from "../../shared/services/lang.service";
import {SidenavService} from "../../shared/services/sidenav.service";
import {TranslationService} from "../../shared/services/translation.service";
import {langMaps} from './sidenav.text';
import {Router} from "@angular/router";
import {UserService} from "../../shared/services/user.service";

@Component({
  selector: 'eibo-client-sidenav',
  templateUrl: './client-sidenav.component.html',
  styleUrls: ['./client-sidenav.component.less']
})
export class ClientSidenavComponent implements OnInit {
  public lang;
  public isAuth: boolean = false;
  public user$ = null;

  constructor(private translationSvc: TranslationService,
              private langSvc: LangService,
              private userSvc: UserService,
              private sidenavSvc: SidenavService,
              private router: Router) {
    translationSvc.initViewTranslation('sidenav', langMaps);
    langSvc.lang$.subscribe(lang => {
      this.lang = lang;
    });
    this.user$ = this.userSvc.user$;
  }

  ngOnInit() {
    this.userSvc.auth$.subscribe(auth => {
      this.isAuth = auth;
    });
  }

  setLang(lang) {
    this.langSvc.lang = lang;
    this.sidenavSvc.close();
  }

  fbLogin() {
    this.userSvc.loginWithFacebook();
  }

  logout() {
    this.sidenavSvc.close();
    this.userSvc.logout()
      .then(() => {
        this.router.navigate(['/']);
      });
  }

  closeSideBar() {
    this.sidenavSvc.close();
  }

  translate(key) {
    return this.translationSvc.translate(key);
  }
}
