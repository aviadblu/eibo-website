const en = {
  lang: 'en',
  "aboutLink": 'About',
  "adminLink": 'Admin',
  "contactLink": 'Contact Us',
  "teamLink": 'Team',
  "facebookLogin": 'Login With Facebook',
  "logout": "Logout"
};

const he = {
  lang: 'he',
  "aboutLink": 'אודות',
  "contactLink": 'צור קשר',
  "adminLink": 'מנהל',
  "teamLink": 'אנחנו',
  "facebookLogin": 'התחבר באמצעות פייסבוק',
  "logout": 'התנתק'
};

export const langMaps = [en,he];
