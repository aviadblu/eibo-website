import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AdminCategoriesService} from "../../../shared/services/admin-categories.service";
import {fromEvent, Observable} from "rxjs";
import {MatDialog} from "@angular/material/dialog";
import {AdminDeleteCategoryComponent} from "../admin-delete-category/admin-delete-category.component";
import {AdminAddCategoryComponent} from "app/client/client-admin-main/admin-add-category/admin-add-category.component";
import {debounceTime, distinctUntilChanged} from "rxjs/internal/operators";

@Component({
  selector: 'eibo-admin-categories',
  templateUrl: './admin-categories.component.html',
  styleUrls: ['./admin-categories.component.less']
})
export class AdminCategoriesComponent implements OnInit {
  displayedColumns = ['id', 'name', 'edit'];
  dataSource;

  @ViewChild('filter') filter: ElementRef;

  constructor(private adminCategoriesSvc: AdminCategoriesService,
              private dialog: MatDialog) {
    adminCategoriesSvc.init()
      .catch(err => {
        console.log(err);
      });
  }

  ngOnInit() {
    this.dataSource = this.adminCategoriesSvc.getAdminCategoriesDataSource();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150),
      distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  addCategory() {
    this.dialog.open(AdminAddCategoryComponent, {
      data: {
        type: 'add'
      }
    });
  }

  editCategory(categoryId) {
    this.dialog.open(AdminAddCategoryComponent, {
      data: {
        type: 'edit',
        categoryId: categoryId
      }
    });
  }

  deleteCategory(categoryId, categoryName) {
    let dialogRef = this.dialog.open(AdminDeleteCategoryComponent, {
      data: {
        categoryId: categoryId,
        categoryName: categoryName
      }
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.adminCategoriesSvc.deleteCategory(categoryId);
      }
    });
  }

}
