import {Component, OnInit} from '@angular/core';
import {AdminBooksService} from "../../shared/services/admin-books.service";

@Component({
  selector: 'eibo-client-admin-main',
  template: `
    <div class="client-site-content">
      <div class="mdl-grid">
        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp page-content">
          <mat-tab-group>
            <mat-tab label="Pending Books ({{pendingBooksCounter}})">
              <eibo-admin-books [pending]="true"></eibo-admin-books>
            </mat-tab>
            <mat-tab label="All Books">
              <eibo-admin-books></eibo-admin-books>
            </mat-tab>
            <mat-tab label="Categories">
              <eibo-admin-categories></eibo-admin-categories>
            </mat-tab>
            <mat-tab label="Groups">
              <eibo-admin-groups></eibo-admin-groups>
            </mat-tab>
          </mat-tab-group>
        </div>
      </div>
    </div>
  `
})
export class ClientAdminMainComponent implements OnInit {
  pendingBooksCounter: number = 0

  constructor(private adminBooksSvc: AdminBooksService) {
    adminBooksSvc.pendingBooks$.subscribe(pendingBooks => {
      this.pendingBooksCounter = pendingBooks.length;
    });
  }

  ngOnInit() {
  }

}
