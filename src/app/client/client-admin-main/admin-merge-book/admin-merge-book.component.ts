import {Component, OnInit, Inject} from '@angular/core';
import {Subject, Observable, of} from "rxjs";
import {SearchResultBook} from "../../../shared/models/search-result-book";
import {MyLibraryService} from "../../../shared/services/my-library.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {AdminBooksService} from "app/shared/services/admin-books.service";
import {catchError, debounceTime, distinctUntilChanged, switchMap, take} from "rxjs/internal/operators";

@Component({
  selector: 'eibo-admin-merge-book',
  templateUrl: './admin-merge-book.component.html',
  styleUrls: ['./admin-merge-book.component.less']
})
export class AdminMergeBookComponent implements OnInit {
  private SearchTerm = new Subject<string>();
  books$: Observable<SearchResultBook[]>;
  replaceWithBook;
  currentBook;

  constructor(private myLibraryService: MyLibraryService,
              private adminBooksService: AdminBooksService,
              public dialogRef: MatDialogRef<AdminMergeBookComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.loadCurrentBookData(data.bookId);
  }

  loadCurrentBookData(bookId) {
    this.myLibraryService.getBook(bookId).pipe(take(1)).subscribe(data => {
      this.currentBook = data[0];
    });
  }

  ngOnInit() {
    this.books$ = this.SearchTerm
      .pipe(debounceTime(300), distinctUntilChanged(),
      switchMap(term =>
        term ? this.myLibraryService.getBooksAutocomplete(term) : of<SearchResultBook[]>([])
      ),
      catchError(error => {
        console.log(error);
        return of<SearchResultBook[]>([])
      }));
  }

  public search(term: string) {
    this.SearchTerm.next(term);
  }

  public bookSelected(e, book) {
    this.replaceWithBook = book;
  }

  public merge() {
    this.adminBooksService.mergeBooks(this.currentBook.id, this.replaceWithBook.id);
  }

}
