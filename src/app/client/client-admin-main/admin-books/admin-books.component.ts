import {Component, ElementRef, OnInit, ViewChild, Input} from '@angular/core';
import {fromEvent, Observable} from "rxjs";
import {AdminBooksService} from "../../../shared/services/admin-books.service";






import {MatDialog} from "@angular/material";
import {AdminAddBookComponent} from "app/client/client-admin-main/admin-add-book/admin-add-book.component";
import {AdminDeleteBookComponent} from "../admin-delete-book/admin-delete-book.component";
import {AdminCategoriesService} from "app/shared/services/admin-categories.service";
import {AdminMergeBookComponent} from "app/client/client-admin-main/admin-merge-book/admin-merge-book.component";
import {debounceTime, distinctUntilChanged, take} from "rxjs/internal/operators";

@Component({
  selector: 'eibo-admin-books',
  templateUrl: './admin-books.component.html',
  styleUrls: ['./admin-books.component.less']
})
export class AdminBooksComponent implements OnInit {

  displayedColumns = ['id', 'name', 'author', 'category', 'cover_url', 'pending', 'merge_identical', 'edit', 'delete'];
  dataSource;
  categories = {0: 'N/A'};

  @ViewChild('filter') filter: ElementRef;

  @Input() pending: boolean = false;

  constructor(private adminBooksSvc: AdminBooksService,
              private adminCategoriessSvc: AdminCategoriesService,
              private dialog: MatDialog) {
    adminBooksSvc.init();
    adminCategoriessSvc.init()
      .then(this.loadCategories.bind(this));
  }

  ngOnInit() {
    this.dataSource = this.adminBooksSvc.getAdminBooksDataSource(this.pending);
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(debounceTime(150))
      .pipe(distinctUntilChanged())
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  private loadCategories() {
    this.adminCategoriessSvc.categories$
      .pipe(take(1))
      .subscribe(categories => {
        categories.forEach(cat => {
          this.categories[cat.id] = cat.name;
        });
      });
  }

  addBook() {
    this.dialog.open(AdminAddBookComponent, {
      data: {
        type: 'add'
      }
    });
  }

  mergeBook(bookId) {
    this.dialog.open(AdminMergeBookComponent, {
      width: '350px',
      data: {
        bookId: bookId
      }
    });
  }

  editBook(bookId) {
    this.dialog.open(AdminAddBookComponent, {
      data: {
        type: 'edit',
        bookId: bookId
      }
    });
  }

  deleteBook(bookId, bookName) {
    let dialogRef = this.dialog.open(AdminDeleteBookComponent, {
      data: {
        bookId: bookId,
        bookName: bookName
      }
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.adminBooksSvc.deleteBook(bookId);
      }
    });
  }

}
