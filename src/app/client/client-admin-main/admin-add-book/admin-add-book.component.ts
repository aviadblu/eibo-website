import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CloudinaryHelperService } from "../../../shared/services/cloudinary-helper.service";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { AdminBooksService } from "../../../shared/services/admin-books.service";
import { AdminCategoriesService } from "../../../shared/services/admin-categories.service";
import {take} from "rxjs/internal/operators";

@Component({
  selector: "eibo-admin-add-book",
  templateUrl: "./admin-add-book.component.html",
  styleUrls: ["./admin-add-book.component.less"]
})
export class AdminAddBookComponent implements OnInit {
  newBookForm: FormGroup;
  cover: string = "";
  ready: boolean = false;
  dialogType: string = "add";
  categories: any[] = [];
  categoriesMap = { 0: "N/A" };

  constructor(
    private fb: FormBuilder,
    private adminBooksSvc: AdminBooksService,
    private adminCategoriesSvc: AdminCategoriesService,
    private dialogRef: MatDialogRef<AdminAddBookComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.adminCategoriesSvc.categories$.pipe(take(1)).subscribe(categories => {
      this.categories = categories;
      categories.forEach(cat => {
        this.categoriesMap[cat.id] = cat.name;
      });
    });

    if (data.type == "edit") {
      this.dialogType = "edit";
      this.adminBooksSvc.loadBookData(data.bookId).then(bookData => {
        this.cover = bookData["thumbnail"] || "";
        this.createNewBookForm(bookData);
      });
    } else {
      this.createNewBookForm({});
    }
  }

  ngOnInit() {}

  private createNewBookForm(data) {
    this.newBookForm = this.fb.group({
      cover_url: [data.cover_url || ""],
      name: [data.name || "", Validators.required],
      author: [data.author || "", Validators.required],
      category: [data.category || 0, Validators.required],
      sku: [data.sku || ""],
      summary: [data.summary || ""],
      pending: [data.pending || false]
    });
    this.ready = true;
  }

  public onSubmitNewBookForm() {
    if (this.newBookForm.valid) {
      if (this.dialogType == "edit") {
        this.adminBooksSvc
          .updateBook(this.data.bookId, this.newBookForm.value)
          .then(() => {
            this.dialogRef.close();
          });
      } else {
        this.adminBooksSvc.addBook(this.newBookForm.value).then(() => {
          this.dialogRef.close();
        });
      }
    }
  }

  updateImage(new_url: string) {
    this.newBookForm.controls["cover_url"].setValue(new_url);
    this.cover = new_url && new_url.indexOf("https://res.cloudinary.com")
      ? CloudinaryHelperService.getUrlWithImageResize(new_url, 92, 140)
      : new_url;
  }
}
