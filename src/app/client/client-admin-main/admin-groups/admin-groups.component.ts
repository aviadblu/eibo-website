import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatTableDataSource} from "@angular/material";
import {AdminGroupsService} from "../../../shared/services/admin-groups-service";
import {Community, CommunityService} from "../../../shared/services/community.service";

@Component({
  selector: 'eibo-admin-groups',
  templateUrl: './admin-groups.component.html',
  styleUrls: ['./admin-groups.component.less']
})
export class AdminGroupsComponent implements OnInit {

  displayedColumns = ['id', 'name', 'logo', 'owner_id', 'address', 'approve', 'delete'];
  private groups: Community[];
  groupsDataSource: MatTableDataSource<Community>;

  constructor(private adminGroupsService: AdminGroupsService,
              private communityService: CommunityService) {
  }

  applyFilter(filterValue: string) {
    this.groupsDataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.adminGroupsService.adminGroups$.subscribe((groups : Community[]) => {
      this.groups = groups;
      this.groupsDataSource = new MatTableDataSource(this.groups);
    });
  }

  saveGroup(group :Community) {
    this.adminGroupsService.approveCommunity(group).subscribe(() => {
      this.removeGroupFromDS(group);
    });
  }

  deleteGroup(group :Community) {
    this.communityService.deleteCommunity(group).subscribe(() => {
      this.removeGroupFromDS(group);
    });
  }

  private removeGroupFromDS(group: Community) {
    this.groups.splice(this.groups.indexOf(group), 1);
    this.groupsDataSource = new MatTableDataSource<Community>(this.groups);
  }

  trackByFunc(c: Community) {
    return c.id;
  }
}
