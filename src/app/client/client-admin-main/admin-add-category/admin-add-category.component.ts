import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AdminCategoriesService} from "../../../shared/services/admin-categories.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'eibo-admin-add-category',
  templateUrl: './admin-add-category.component.html',
  styleUrls: ['./admin-add-category.component.less']
})
export class AdminAddCategoryComponent implements OnInit {
  newCategoryForm: FormGroup;
  ready: boolean = false;
  dialogType: string = 'add';

  constructor(private fb: FormBuilder,
              private adminCategoriesSvc: AdminCategoriesService,
              private dialogRef: MatDialogRef<AdminAddCategoryComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    if (data.type == 'edit') {
      this.dialogType = 'edit';
      this.adminCategoriesSvc.loadCategoryData(data.categoryId)
        .then(categoryData => {
          this.createNewCategoryForm(categoryData);
        });
    } else {
      this.createNewCategoryForm({});
    }
  }

  ngOnInit() {
  }


  private createNewCategoryForm(data) {
    this.newCategoryForm = this.fb.group({
      name: [data.name || '', Validators.required]
    });
    this.ready = true;
  }

  public onSubmitNewCategoryForm() {
    if (this.newCategoryForm.valid) {
      if (this.dialogType == 'edit') {
        this.adminCategoriesSvc.updateCategory(this.data.categoryId, this.newCategoryForm.value)
          .then(() => {
            this.dialogRef.close();
          });
      } else {
        this.adminCategoriesSvc.addCategory(this.newCategoryForm.value)
          .then(() => {
            this.dialogRef.close();
          });
      }
    }
  }
}
