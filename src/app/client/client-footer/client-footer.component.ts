import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'eibo-client-footer',
  template: `
    <footer class="mdl-mini-footer eibo-gray-background mdl-cell--hide-phone">
      <div class="footer-container">
        <div class="mdl-logo">&copy; EiBO 2017</div>
        <ul class="mdl-mini-footer__link-list">
          <li>

            <a class="footer-social-icon" href="https://www.linkedin.com/company-beta/18188638/" target="_blank">
              <img src="/assets/img/linkedin-icon.png">
            </a>

            <a class="footer-social-icon" href="https://www.facebook.com/groups/149390488919296/" target="_blank">
              <img src="/assets/img/facebook-icon.png">
            </a>

          </li>
        </ul>
      </div>
    </footer>
  `,
  styles: [`
    footer {
      opacity: 0.9;
      box-sizing: border-box;
      width: 100%;
      padding: 5px 16px;
      position: fixed;
      bottom: 0px;
      z-index: 10;
    }

    .footer-container {
      box-sizing: border-box;
      margin: 0 auto;
    }
    
    .footer-social-icon img {
      height: 30px;
      width: 30px;
    }

  `]
})
export class ClientFooterComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
