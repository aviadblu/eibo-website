import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';

import {TranslationService} from '../../shared/services/translation.service';
import {ContactService} from '../../shared/services/contact.service';
import {langMaps} from './contact.text';

@Component({
  selector: 'eibo-client-contact',
  templateUrl: './client-contact.component.html',
  styleUrls: ['./client-contact.component.less']
})

export class ClientContactComponent implements OnInit {
  contactForm: FormGroup;
  isSent: boolean = false;
  isSuccess: boolean = false;

  constructor(private translationSvc: TranslationService, public snackBar: MatSnackBar, private contactService: ContactService, fb: FormBuilder) {
    translationSvc.initViewTranslation('contact', langMaps);
    this.contactForm = fb.group({
      'name': ['', Validators.required],
      'email': ['', Validators.email],
      'message': ['', Validators.required]
    });
  }


  ngOnInit() {

  }

  openSnackBar(text: string) {
    this.snackBar.open(text, "close", {duration: 2000});
  }

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      this.contactService
        .sendContact(form.value)
        .then(() => {
          this.openSnackBar("Sent successfully!");
          this.isSent = true;
          this.isSuccess = true;
        })
        .catch(reason => {
          console.error(reason);
          this.openSnackBar("Error: please try again later");
          this.isSent = true;
          this.isSuccess = false;
        });
    }
  }

  translate(key) {
    return this.translationSvc.translate(key);
  }

}
