const en = {
  lang: 'en',
  "pageHeadline": `Contact Us`,
  "name": 'Name',
  "email": 'Email',
  "message": 'Message',
  "send": 'send',
  "messageSentMessage": `Message sent successfully!`,
  "getBackMessage": 'we will get back to you as soon as possible'
};

const he = {
  lang: 'he',
  "pageHeadline": `צור קשר`,
  "name": 'שם',
  "email": 'כתובת אימייל',
  "message": 'הודעה',
  "send": 'שלח',
  "messageSentMessage": `ההודעה נשלחה בהצלחה!`,
  "getBackMessage": 'נחזור אליך בהקדם האפשרי'
};

export const langMaps = [en,he];
