import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {UserBookService} from "../../shared/services/user-book.service";
import {Book} from "../../shared/models/book";

@Component({
  selector: 'eibo-book-review-dialog',
  templateUrl: './book-review-dialog.component.html',
  styleUrls: ['./book-review-dialog.component.less']
})
export class BookReviewDialogComponent implements OnInit {
  form: FormGroup;
  isSubmit: boolean;

  constructor(public dialogRef: MatDialogRef<BookReviewDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {userBook :Book},
              private fb: FormBuilder,
              private userBookSvc :UserBookService) {
    this.form = this.fb.group({
      rating: [data.userBook.rating],
      review: [data.userBook.review]
    });
  }


  public setRating(rating) {
    this.form.get('rating').setValue(rating);
  }

  public saveChanges() {
    this.data.userBook.rating = this.form.get('rating').value;
    this.data.userBook.review = this.form.get('review').value;
    this.userBookSvc.editUserBook(this.data.userBook.bookuserid, this.data.userBook).subscribe(() => {
      this.isSubmit = false;
      this.dialogRef.close(this.data.userBook);
    });
  }

  close() {
    this.dialogRef.close();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    $event.preventDefault();
    return false;
  }

  ngOnInit() {
  }
}
