import {AfterContentInit, Component, OnInit, ViewChild} from '@angular/core';
import {User} from "../types/user.module";
import {UserService} from "../../shared/services/user.service";
import {FacebookService} from "../../shared/services/facebook.service";

@Component({
  selector: 'eibo-friends-slider',
  templateUrl: './friends-slider.component.html',
  styleUrls: ['./friends-slider.component.less']
})
export class FriendsSliderComponent implements OnInit {
  friends: User[] = [];

  constructor(private userService: UserService) {
    let self = this;
    this.userService.userLoggedIn$.subscribe((user) => {
      if (user) {
        self.userService.getUserFriends((friends) => {
          self.friends = friends;
        });
      }
    });
  }

  ngOnInit() {
  }
}
