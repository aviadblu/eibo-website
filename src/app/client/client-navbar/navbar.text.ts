const en = {
  lang: 'en',
  "aboutLink": 'About',
  "teamLink": 'Team',
  "contactLink": 'Contact Us',
  "facebookLogin": 'Login With Facebook',
};

const he = {
  lang: 'he',
  "aboutLink": 'אודות',
  "teamLink": 'אנחנו',
  "contactLink": 'צור קשר',
  "facebookLogin": 'התחבר באמצעות פייסבוק',
};

export const langMaps = [en,he];
