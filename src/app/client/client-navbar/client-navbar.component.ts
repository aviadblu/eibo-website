import { Subject,  Observable } from 'rxjs';
import { MyLibraryService } from '../../shared/services/my-library.service';
import { SearchResultBook } from '../../shared/models/search-result-book';
import {Component, OnInit, OnDestroy, AfterContentChecked, AfterViewInit} from '@angular/core';
import {langMaps} from './navbar.text';
import {TranslationService} from "../../shared/services/translation.service";
import {SidenavService} from "app/shared/services/sidenav.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../shared/services/user.service";
import { ChatsService } from 'app/shared/services/chats.service';
import { MatDialog } from '@angular/material';
import { ClientReportComponent } from '../client-report/client-report.component';
import {Constants} from "../../constants";

@Component({
  selector: 'eibo-client-navbar',
  templateUrl: './client-navbar.component.html',
  styleUrls: ['./client-navbar.component.less']
})
export class ClientNavbarComponent implements OnInit, OnDestroy, AfterViewInit {
  public state: string = 'loading';
  private sub = [];
  public errors = [];
  public user$ = null;
  public unread$;
  tutorial:boolean;
  toolBarPosition = "fixed";
  EIBO_LOGO = Constants.EIBO_LOGO;

  constructor(
              public dialog: MatDialog ,
              private translationSvc: TranslationService,
              private sidenavSvc: SidenavService,
              private myLibraryService: MyLibraryService,
              public userSvc: UserService,
              private route: ActivatedRoute,
              private chatSvc: ChatsService,
              private router: Router) {
    translationSvc.initViewTranslation('navbar', langMaps);
    this.unread$ = this.chatSvc.unread$;
  }

  ngAfterViewInit() {
    this.user$ = this.userSvc.user$;
  }

  fbLogin() {
    this.state = 'loading';
    this.userSvc.loginWithFacebook();
  }

  ngOnInit() {

    this.userSvc.user$
    .subscribe(userData => {
      if (userData && 'app_options' in userData) {
        if ('profileWizardSkipped' in userData.app_options) {
          this.userSvc.tutorial.home = false;
          this.userSvc.tutorial.nav = false;
        }
      }
    });
    let self = this;
    if(this.router.url.indexOf('chat/')>0) {

      this.toolBarPosition = "relative";
    } else {

      this.toolBarPosition = "fixed";
    }

    this.router.events.subscribe(()=>{

      if(this.router.url.indexOf('chat')>0) {

        this.toolBarPosition = "relative";
      } else {

        this.toolBarPosition = "fixed";
      }
    })
    this.sub[0] = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        if (params['logout']) {
          this.state = 'notauth';
          self.router.navigate(['/app/home']);
        }
      });

    this.sub[1] = this.userSvc.auth$.subscribe(auth => {
      if (!auth) {
        this.state = 'notauth';
      } else {
        this.state = 'auth';
      }
    });

  }
  openReport() {
    let dialogRef = this.dialog.open(ClientReportComponent,{minWidth:360});
  }
  ngOnDestroy() {
    this.sub.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  translate(key) {
    return this.translationSvc.translate(key);
  }

  openSidenav() {
    this.sidenavSvc.toggle();
  }

}
