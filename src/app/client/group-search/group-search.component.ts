import { Component, OnInit } from '@angular/core';
import {CommunityService} from "../../shared/services/community.service";

@Component({
  selector: 'eibo-group-search',
  templateUrl: './group-search.component.html',
  styleUrls: ['./group-search.component.less']
})
export class GroupSearchComponent implements OnInit {
  getGroupsObservable: any;
  groups = [{name: 'group1'}, {name: 'group2'}];

  constructor(private communityService: CommunityService) { }

  ngOnInit() {
    this.getGroupsObservable = this.communityService.getCommunityAutoComplete.bind(this.communityService);
  }

}
