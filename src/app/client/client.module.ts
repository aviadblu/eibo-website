import {AddCommunityComponent} from './client-u-profile/communities/add-community/add-community.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from '../app-routing.module';
import {MaterialModule} from '../material/material.module';
import {ContactService} from '../shared/services/contact.service';
import {FirebaseService} from '../shared/services/firebase.service';
import {MyLibraryService} from '../shared/services/my-library.service';
import {SharedModule} from '../shared/shared.module';
import {ClientAboutComponent} from './client-about/client-about.component';
import {ClientContactComponent} from './client-contact/client-contact.component';
import {ClientFooterComponent} from './client-footer/client-footer.component';
import {ClientHomepageComponent} from './client-homepage/client-homepage.component';
import {ClientMainComponent} from './client-main/client-main.component';
import {ClientNavbarComponent} from './client-navbar/client-navbar.component';
import {ClientSidenavComponent} from './client-sidenav/client-sidenav.component';
import {ClientTeamComponent} from './client-team/client-team.component';
import {ClientUProfileComponent} from './client-u-profile/client-u-profile.component';
import {ClientUComponent} from './client-u/client-u.component';
import {UserBookService} from "../shared/services/user-book.service";
import {UserProfileComponent} from "./client-u-profile/profile/user-profile.component";
import {CommunitiesComponent} from "./client-u-profile/communities/communities.component";
import {MyLibraryComponent} from "./client-u-profile/my-library/my-library.component";
import {BooksSliderComponent} from "./client-u-profile/my-library/books-slider/books-slider.component";
import {AddBookComponent} from "./client-u-profile/my-library/add-book/add-book.component";
import {CloudinaryHelperService} from "../shared/services/cloudinary-helper.service";
import {ClientAdminComponent} from './client-admin/client-admin.component';
import {ClientAdminMainComponent} from './client-admin-main/client-admin-main.component';
import {AdminBooksComponent} from './client-admin-main/admin-books/admin-books.component';
import {AdminCategoriesComponent} from './client-admin-main/admin-categories/admin-categories.component';
import {AdminBooksService} from "../shared/services/admin-books.service";
import {FileUploadModule} from "ng2-file-upload";
import {AdminAddBookComponent} from "./client-admin-main/admin-add-book/admin-add-book.component";
import {AdminDeleteBookComponent} from './client-admin-main/admin-delete-book/admin-delete-book.component';
import {ClientProfileWizardComponent} from './client-profile-wizard/client-profile-wizard.component';
import {GmapService} from "../shared/services/gmap.service";
import {AdminCategoriesService} from "app/shared/services/admin-categories.service";
import {AdminDeleteCategoryComponent} from "./client-admin-main/admin-delete-category/admin-delete-category.component";
import {AdminAddCategoryComponent} from './client-admin-main/admin-add-category/admin-add-category.component';
import {AdminMergeBookComponent} from './client-admin-main/admin-merge-book/admin-merge-book.component';
import {ClientProfileComponent} from "./client-profile/client-profile.component";
import {MyCommunitiesComponent} from "app/client/client-u-profile/communities/my-communities/my-communities.component";
import {CommunityService} from "../shared/services/community.service";
import {ChatsService} from "../shared/services/chats.service";
import {ClientChatComponent} from './client-chat/client-chat.component';
import {ClientChatsListComponent} from './client-chats-list/client-chats-list.component';
import {GeneralService} from 'app/shared/services/general.service';
import {UserService} from "../shared/services/user.service";
import {CloudinaryModule, CloudinaryConfiguration} from '@cloudinary/angular-5.x';
import {Cloudinary} from 'cloudinary-core';
import {ClientBookComponent} from './client-book/client-book.component';
import {ReviewBookComponent} from './client-u-profile/my-library/review-book/review-book.component';
import {FacebookService} from "../shared/services/facebook.service";
import { ClientReportComponent } from './client-report/client-report.component';
import { ClientSearchComponent } from './client-search/client-search.component';
import { ClientSliderComponent } from './client-slider/client-slider.component';
import { AddLibraryBookComponent } from './client-u-profile/my-library/add-library-book/add-library-book.component';
import { BookSelectorComponent } from './client-u-profile/my-library/book-selector/book-selector.component';
import { AddBookPanelComponent } from './client-u-profile/my-library/add-library-book/add-book-panel/add-book-panel.component';
import {BarcodeScannerComponent} from "../shared/components/barcode-scanner/barcode-scanner.component";
import {GeolocationFieldComponent} from "./client-u-profile/geolocation-field/geolocation-field.component";
import { GroupsSuggestionsComponent } from './groups-suggestions/groups-suggestions.component';
import { ClientCommunityComponent } from './client-u-profile/communities/client-community/client-community.component';
import { FriendsSliderComponent } from './friends-slider/friends-slider.component';
import { CreateCommunityComponent } from './create-community/create-community.component';
import { CommunitiesListComponent } from './client-u-profile/communities/communities-list/communities-list.component';
import { CommunityCardComponent } from './client-u-profile/communities/community-card/community-card.component';
import { AdminGroupsComponent } from './client-admin-main/admin-groups/admin-groups.component';
import {AdminGroupsService} from "../shared/services/admin-groups-service";
import {FacebookModule} from "ngx-facebook";
import { UserBubbleComponent } from './user-bubble/user-bubble.component';
import { UsersGridListComponent } from './users-grid-list/users-grid-list.component';
import { GroupSearchComponent } from './group-search/group-search.component';
import { MainSearchComponent } from './main-search/main-search.component';
import {IconsService} from "../shared/icons-service.service";
import { ReadMoreDirective } from './read-more.directive';
import { BookReviewDialogComponent } from './book-review-dialog/book-review-dialog.component';
import {UserLeavesGuard} from "../guards/user-leaves-guard.guard";
import { BookSearchResultComponent } from './book-search-result/book-search-result.component';
import { GroupBooksSearcherComponent } from './group-books-searcher/group-books-searcher.component';
import {LoginGuard} from "../guards/login-guard.guard";
import { UsersSliderComponent } from './users-slider/users-slider.component';
import {PickerModule} from "@ctrl/ngx-emoji-mart";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    FacebookModule.forRoot(),
    CloudinaryModule.forRoot({Cloudinary}, {cloud_name: 'db5v7kkij'} as CloudinaryConfiguration),
    PickerModule
  ],
  declarations: [
    ClientMainComponent,
    ClientNavbarComponent,
    ClientFooterComponent,
    ClientHomepageComponent,
    ClientAboutComponent,
    ClientTeamComponent,
    ClientSidenavComponent,
    ClientUProfileComponent,
    ClientUComponent,
    ClientContactComponent,
    UserProfileComponent,
    CommunitiesComponent,
    MyLibraryComponent,
    BooksSliderComponent,
    AddBookComponent,
    ReviewBookComponent,
    ClientAdminComponent,
    ClientAdminMainComponent,
    AdminBooksComponent,
    AdminCategoriesComponent,
    AdminAddBookComponent,
    AdminDeleteBookComponent,
    ClientProfileWizardComponent,
    AdminDeleteCategoryComponent,
    AdminAddCategoryComponent,
    AdminMergeBookComponent,
    ClientProfileComponent,
    MyCommunitiesComponent,
    AddCommunityComponent,
    ClientChatComponent,
    ClientChatsListComponent,
    ClientBookComponent,
    ClientReportComponent,
    ClientSearchComponent,
    ClientSliderComponent,
    AddLibraryBookComponent,
    BookSelectorComponent,
    AddBookPanelComponent,
    BarcodeScannerComponent,
    GeolocationFieldComponent,
    GroupsSuggestionsComponent,
    ClientCommunityComponent,
    FriendsSliderComponent,
    CreateCommunityComponent,
    CommunitiesListComponent,
    CommunityCardComponent,
    AdminGroupsComponent,
    UserBubbleComponent,
    UsersGridListComponent,
    GroupSearchComponent,
    MainSearchComponent,
    ReadMoreDirective,
    BookReviewDialogComponent,
    BookSearchResultComponent,
    GroupBooksSearcherComponent,
    UsersSliderComponent
  ],
  entryComponents: [
    AddBookComponent,
    ReviewBookComponent,
    AdminAddBookComponent,
    AdminDeleteBookComponent,
    ClientProfileWizardComponent,
    AddCommunityComponent,
    AdminDeleteCategoryComponent,
    AdminAddCategoryComponent,
    AdminMergeBookComponent,
    ClientReportComponent
  ],
  exports: [
    ClientMainComponent,
    ClientHomepageComponent,
    ClientAboutComponent,
    ClientTeamComponent,
    ClientUProfileComponent,
    ClientUComponent,
    MaterialModule,
    ClientAdminComponent,
    ClientAdminMainComponent,
    ClientProfileComponent,
    MyCommunitiesComponent,
    ClientChatComponent,
    ClientChatsListComponent,
    BarcodeScannerComponent
  ],
  providers: [
    FacebookService,
    IconsService,
    UserService,
    FirebaseService,
    ContactService,
    MyLibraryService,
    UserBookService,
    CloudinaryHelperService,
    GmapService,
    AdminBooksService,
    AdminGroupsService,
    AdminCategoriesService,
    CommunityService,
    ChatsService,
    GeneralService,
    UserLeavesGuard,
    LoginGuard
  ]
})
export class ClientModule {
}
