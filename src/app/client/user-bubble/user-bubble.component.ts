import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'eibo-user-bubble',
  templateUrl: './user-bubble.component.html',
  styleUrls: ['./user-bubble.component.less']
})
export class UserBubbleComponent implements OnInit {
  @Input() user;

  constructor() { }

  ngOnInit() {
  }

}
