import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';

import {ContactService} from '../../shared/services/contact.service';
@Component({
  selector: 'eibo-client-report',
  templateUrl: './client-report.component.html',
  styleUrls: ['./client-report.component.less']
})
export class ClientReportComponent implements OnInit {
  issueForm: FormGroup;
  isSent: boolean = false;
  isSuccess: boolean = false;

  constructor(public snackBar: MatSnackBar,  private contactService: ContactService, fb: FormBuilder) {
    this.issueForm = fb.group({
      'name': ['', Validators.required],
      'email': ['', Validators.email],
      'issue': ['', Validators.required],
      'message': ['', Validators.required]
    });
  }

  openSnackBar(text: string) {
    this.snackBar.open(text, "close", {duration: 2000});
  }

  onSubmit(form: FormGroup): void {
    if (form.valid) {
      form.value.message = form.value.issue + "  -  " + form.value.message;
      this.contactService
        .sendReport(form.value)
        .then(() => {
          this.openSnackBar("Sent successfully!");
          this.isSent = true;
          this.isSuccess = true;
        })
        .catch(reason => {
          console.error(reason);
          this.openSnackBar("Error: please try again later");
          this.isSent = true;
          this.isSuccess = false;
        });
    }
  }

  ngOnInit() {
  }

}
