import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersGridListComponent } from './users-grid-list.component';

describe('UsersGridListComponent', () => {
  let component: UsersGridListComponent;
  let fixture: ComponentFixture<UsersGridListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersGridListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersGridListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
