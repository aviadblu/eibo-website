import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {User} from "../types/user.module";
import {UserService} from "../../shared/services/user.service";

@
  Component({
  selector: 'eibo-users-grid-list',
  templateUrl: './users-grid-list.component.html',
  styleUrls: ['./users-grid-list.component.less']
})
export class UsersGridListComponent implements OnInit {
  @Input() users: User[];
  showCount = 4;
  constructor() { }

  ngOnInit() {
  }

  increaseShowCount() {
    this.showCount += 4;
  }

}
