import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupBooksSearcherComponent } from './group-books-searcher.component';

describe('GroupBooksSearcherComponent', () => {
  let component: GroupBooksSearcherComponent;
  let fixture: ComponentFixture<GroupBooksSearcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupBooksSearcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupBooksSearcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
