import {Component, Input, OnInit} from '@angular/core';
import {MyLibraryService} from "../../shared/services/my-library.service";
import {Observable} from "rxjs/index";
import {Book} from "../../shared/models/book";

@Component({
  selector: 'eibo-group-books-searcher',
  templateUrl: './group-books-searcher.component.html',
  styleUrls: ['./group-books-searcher.component.less']
})
export class GroupBooksSearcherComponent implements OnInit {
  getBooksObservable: any;
  @Input() communityId: number;

  constructor(private libSvc : MyLibraryService) {
    this.getBooksObservable = (term) => {
      return this.libSvc.getBooksAutocomplete(term, this.communityId);
    };
  }

  ngOnInit() {
  }

}
