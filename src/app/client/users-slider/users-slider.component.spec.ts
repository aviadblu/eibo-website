import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersSliderComponent } from './users-slider.component';

describe('UsersSliderComponent', () => {
  let component: UsersSliderComponent;
  let fixture: ComponentFixture<UsersSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
