import {AfterContentInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {User} from "../types/user.module";
import {FacebookService} from "../../shared/services/facebook.service";
import {UserService} from "../../shared/services/user.service";

@Component({
  selector: 'eibo-users-slider',
  templateUrl: './users-slider.component.html',
  styleUrls: ['./users-slider.component.less']
})
export class UsersSliderComponent implements OnInit, AfterContentInit {
  @Input() users: User[] = [];
  @ViewChild('whatsappShare') whatsappShare;

  _shareText = window.location.origin + `
EiBO - הספריה החברתית הראשונה המאפשרת להחליף ולהשאיל ספרים בין חברים ובתוך קהילות קוראים.`;

  @Input() set shareText(text) {
    if(text) {
      this._shareText = text;
      this._shareText = encodeURI(this._shareText);
      this.whatsappShare.nativeElement.href = 'whatsapp://send?text=' + this._shareText;
    }
  }
  get shareText() {
    return this._shareText;
  }

  config :SwiperOptions = {
    slidesPerView: 3,
    spaceBetween: 10,
    freeMode: true
  };


  constructor(private userService: UserService,
              private fbService :FacebookService) {
  }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.shareText = (this._shareText);
  }

  shareFacebook() {
    this.fbService.shareApp();
  }
}

