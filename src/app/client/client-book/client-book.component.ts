import { Observable , forkJoin} from 'rxjs';
import { DialogDeleteComponent } from '../../shared/components/dialog-delete/dialog-delete.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import { MyLibraryService } from 'app/shared/services/my-library.service';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog, MatTooltip} from '@angular/material'
import { ChatsService } from 'app/shared/services/chats.service';
import { UserService } from 'app/shared/services/user.service';
import {User} from "../types/user.module";
import {GmapService} from "../../shared/services/gmap.service";
import {Constants} from "../../constants";
import {map, skip} from "rxjs/internal/operators";
import {FacebookService} from "../../shared/services/facebook.service";

@Component({
  selector: 'eibo-client-book',
  templateUrl: './client-book.component.html',
  styleUrls: ['./client-book.component.less']
})
export class ClientBookComponent implements OnInit {
  book;
  reviews;
  followed: boolean = false;
  EIBO_LOGO = Constants.EIBO_LOGO;
  @ViewChild('swapTooltip') set showSwapTooltip(elment) {
    this.showTooltip(elment)
  };

  constructor(
    public bookServ:MyLibraryService,
    public gmapsSvc :GmapService,
    public userService:UserService,
    public fbService: FacebookService,
    public dialog: MatDialog,
    public chatSvc: ChatsService,
    private router: Router,
    public route:ActivatedRoute) { }

  swap(book, review) {
    /* if(this.userService.user['books_num']>0) { */
        let dialog = this.dialog.open(DialogDeleteComponent, {
          data: {caption: 'Do you want to ask ' + review.username + ' for this book?'}
        });
        dialog.afterClosed().subscribe((confirm) => {
          if (confirm) {
            forkJoin([
              this.userService.loadUserData(review.userid),
              this.bookServ.getBook(book.id),
            ])
            .subscribe(res=>{
              this.chatSvc.checkCreateNewChat(res[0].uid)
                .then((chatId:string) => {
                  this.chatSvc.bookSwap(chatId,
                    {
                      id: res[0].uid,
                      pg: res[0].id,
                      name: res[0].name,
                      photo: res[0].photo,
                      distance: 2.5,
                    },
                    {
                      bookId: res[1].id,
                      bookName: res[1].name,
                      bookAuthor: res[1].author,
                      bookCover: res[1].cover_url,
                      reviewScore: review.rating,
                    }
                  )
                  .then(chatId => {
                    this.router.navigate(['/app/u/chat/', chatId]);
                  })
              });
            })
          }
        });
/*       } else {
        let dialog = this.dialog.open(DialogAlertComponent, {
          data: {caption: 'In order to swap books you need to add books to your library.',route:'/app/u/profile/library'}
        });
        dialog.afterClosed().subscribe((route) => {
          if (route) {
            this.router.navigate([route]);
          }
        });
    } */
  }
  escapeHtml(text) {
    var map = {
       "&amp;":"&",
       "&lt;":"<",
       "&gt;":">",
       "&quot;":'"',
       "&#039;":"'"
    };

    return text.replace(/[&<>"']/g, function(m) {
      return map[m];
    });
  }
  ngOnInit() {
    let self = this;
    self.route.params.subscribe(params => {
      self.bookServ.getBook(params.bookId).subscribe(book=>{
        self.book = book;
        self.bookServ.getBookReviews(params.bookId).subscribe((res: {data: User[]}) =>{
          self.reviews = res.data;

          self.fbService.isLoggedIn$.getValue() === true ||
              self.fbService.isLoggedIn$.pipe(skip(1)).pipe(map((val) => {
                self.gmapsSvc.updateUsersDistanceToUser(this.reviews);
                self.bookServ.isBookFollowed(book).subscribe((followed) => {
                  self.followed = followed
                })
              }));
        });
      });
    });
  }

  private showTooltip(swapTooltip: MatTooltip) {
    this.userService.app_options$.subscribe((app_options) => {
      if (!app_options.profileHideTutorialSwap && swapTooltip && !swapTooltip._isTooltipVisible()) {
        swapTooltip.show();
        this.hideTutorial();
      }
    });
  }

  removeCover(book) {
    book.cover_url = '';
  }

  toggleFollowBook() {
      if(!this.followed) {
        this.bookServ.followBook(this.book).subscribe(() => {
          this.followed = true;
        });
      } else {
        this.bookServ.unfollowBook(this.book).subscribe(() => {
          this.followed = false;
        });
      }
  }

  hideTutorial() {
    this.userService
      .updateApplicationOptions("profileHideTutorialSwap", true)
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  }
}
