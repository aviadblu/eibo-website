import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../../shared/models/book";
import {Constants} from "../../constants";

@Component({
  selector: 'eibo-book-search-result',
  templateUrl: './book-search-result.component.html',
  styleUrls: ['./book-search-result.component.less']
})
export class BookSearchResultComponent implements OnInit {
  @Input() book :Book;
  EIBO_LOGO = Constants.EIBO_LOGO;

  constructor() { }

  ngOnInit() {
  }

}
