import {Component, OnInit} from '@angular/core';
import {UserService} from "../../shared/services/user.service";

@Component({
  selector: 'eibo-client-u',
  template: `
    <div *ngIf="!auth">Please login</div>
    <eibo-loader *ngIf="!user"></eibo-loader>
    <router-outlet *ngIf="auth && user"></router-outlet>
  `,
  styles: [
    `:host {
      display: flex;
      flex-direction: column;
      height: 100%;
    }`
  ]
})
export class ClientUComponent implements OnInit {
  auth = false;
  user = null;

  constructor(private userSvc: UserService) {

    this.userSvc.auth$.subscribe(auth => {
      this.auth = auth;
    });

    this.userSvc.user$.subscribe(userInfo => {
      this.user = userInfo;
    });
  }

  ngOnInit() {
  }

}
