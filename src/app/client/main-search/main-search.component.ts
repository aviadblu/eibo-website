import { Component, OnInit } from '@angular/core';
import {MyLibraryService} from "../../shared/services/my-library.service";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'eibo-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: ['./main-search.component.less']
})
export class MainSearchComponent implements OnInit {
  getBooksObservable: (term: string) => Observable<any>;
  getFriendsObservable: (term: string) => Observable<any>;

  constructor(private libSvc : MyLibraryService) {
    this.getBooksObservable = this.libSvc.getBooksAutocomplete.bind(this.libSvc);
    this.getFriendsObservable = this.libSvc.getUsersAutocomplete.bind(this.libSvc);
  }

  ngOnInit() {
  }
}
