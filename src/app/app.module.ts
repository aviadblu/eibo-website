import 'hammerjs';

import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {
  BrowserModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG
} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ClientModule} from './client/client.module';
import {LangService} from './shared/services/lang.service';
import {SidenavService} from './shared/services/sidenav.service';
import {TranslationService} from './shared/services/translation.service';
import {FileUploadModule} from 'ng2-file-upload'
import {CloudinaryModule, CloudinaryConfiguration} from '@cloudinary/angular-5.x';
import {Cloudinary} from 'cloudinary-core';
import {SharedModule} from "./shared/shared.module";
import {HttpService} from "./shared/services/http.service";
import {UserLeavesGuard} from "./guards/user-leaves-guard.guard";
import {LoginGuard} from "./guards/login-guard.guard";
//@
declare var Hammer: any;
export class MyHammerConfig extends HammerGestureConfig  {
  buildHammer(element: HTMLElement) {
    let mc = new Hammer(element, {
      touchAction: "pan-y"
    });
    return mc;
  }
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    SharedModule,
    CloudinaryModule.forRoot({Cloudinary}, {cloud_name: 'db5v7kkij'} as CloudinaryConfiguration)
  ],
  providers: [
    LangService,
    HttpService,
    TranslationService,
    SidenavService,
    {
      // hammer instantion with custom config
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig ,
    },
    UserLeavesGuard,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
