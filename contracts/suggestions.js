/**
 * @GET api/suggestions/:userId
 * filter will apply on name
 * @return response
 * @return error response
 */
const response = {
  data: [
    {
      bookId: 1,
      bookName: "some name 1",
      bookAuthor: "Author name",
      bookPage: "/book/1",
      reviewScore: 7.5,
      reviewCount: 6,
      users: [
        {
          id: "asasasasass",
          name: "User 1",
          photo: "https://cloudinary.com/asasas/asasas/asas.jpg",
          distance: 2.5,
          profile: "/profile/asasasasasasasa"
        },
        {
          id: "asasasasass",
          name: "User 2",
          photo: "https://cloudinary.com/asasas/asasas/asas.jpg",
          distance: 0.8,
          profile: "/profile/asasasasasasasa"
        }
      ]
    }
  ]
};


const errorResponse = {
  message: "some error message"
};
