/**
 * @GET api/books/booksListNames[?filter=some_text]
 * filter will apply on name and author
 * @return response
 * @return error response
 */
const response = {
  count: 2,
  data: [
    {
      id: 1,
      name: "some name 1",
      author: "some author 1",
      picture: "http://someurl.net"
    },
    {
      id: 2,
      name: "some name 2",
      author: "some author 2",
      picture: "http://someurl.net"
    }
  ]
};

const errorResponse = {
  message: "some error message"
};


/**
 * @GET api/booksUser/:userId
 * filter will apply on name and author
 * @return response
 * @return error response
 */
const response = {
  count: 2,
  data: [
    {
      id: 1,
      name: "some name 1",
      author: "some author 1",
      picture: "http://someurl.net",
      bookUserId: 2,
      rating: 6,
      review: "some review",
      ready_for_swap: true
    },
    {
      id: 2,
      name: "some name 2",
      author: "some author 2",
      picture: "http://someurl.net",
      bookUserId: 3,
      rating: 8,
      review: "some review 2",
      ready_for_swap: true
    }
  ]
};


/**
 * @POST api/userBooks/:userId
 * @payload {
 *  userId: integer,
 *  bookId: integer,
 *  rating: integer[1-10],
 *  review: text[0-500],
 *  ready_for_swap: false
 * }
 * @return response
 * @return error response
 */
const response = {
  id: 1,
  userId: 1,
  bookId: 1,
  rating: 6,
  review: "some review",
  ready_for_swap: false
};


/**
 * @PUT api/userBooks/:bookUserId
 * @payload {
 *  rating: integer[1-10],
 *  review: text[0-500]
 * }
 * @return response
 * @return error response
 */
const response = {
  id: 1,
  userId: 1,
  bookId: 1,
  rating: 6,
  review: "some review",
  ready_for_swap: false
};

/**
 * @DELETE api/userBooks/:bookUserId
 * @return response
 * @return error response
 */

const response = {
  message: "deleted successfully"
};

