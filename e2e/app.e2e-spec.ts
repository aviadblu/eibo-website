import { EiboWebsite2Page } from './app.po';

describe('eibo-website2 App', () => {
  let page: EiboWebsite2Page;

  beforeEach(() => {
    page = new EiboWebsite2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('eibo works!');
  });
});
